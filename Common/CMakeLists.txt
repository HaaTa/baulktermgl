###| CMAKE Common |###

###| Project
project( Common )
cmake_minimum_required( VERSION 2.6 )
set( HEAD_DIR ${PROJECT_SOURCE_DIR}/.. )


##| Baulk Widget

#| Sources
set( SRCS_BW
	./baulkwidget.cpp
)

#| Headers
set( HDRS_BW
	./baulkwidget.h
)

#| Library
set( LIB_BW
	BaulkWidget
)


##| Baulk Window

#| Sources
set( SRCS_BWN
	./baulkwindow.cpp
)

#| Headers
set( HDRS_BWN
	./baulkwindow.h
)

#| Library
set( LIB_BWN
	BaulkWindow
)


##| BaulkXML

#| Sources
set( SRCS_BXML
        ./baulkxml.cpp
)

#| Headers
set( HDRS_BXML
	./baulkxml.h
)

#| Library
set( LIB_BXML
	BaulkXML
)


###| Misc Qt4
add_definitions( 
	-Wall 
)
find_package( Qt4 REQUIRED )
set( QT_USE_QTXML 1 )
include( ${QT_USE_FILE} )


##| Baulk Widget
if ( BuildBaulkWidget )
	QT4_WRAP_CPP( MOC_SRCS_BW ${HDRS_BW} )
	
	if ( WIN32 )
		add_library( ${LIB_BW} STATIC ${SRCS_BW} ${MOC_SRCS_BW} )
	else ( WIN32 )
		add_library( ${LIB_BW} SHARED ${SRCS_BW} ${MOC_SRCS_BW} )
	endif( WIN32 )
	
	target_link_libraries( ${LIB_BW} ${QT_LIBRARIES} )
	install( TARGETS ${LIB_BW} DESTINATION lib )
endif ( BuildBaulkWidget )


##| Baulk Window
if ( BuildBaulkWindow )
	QT4_WRAP_CPP( MOC_SRCS_BWN ${HDRS_BWN} )
	
	if ( WIN32 )
		add_library( ${LIB_BWN} STATIC ${SRCS_BWN} ${MOC_SRCS_BWN} )
	else ( WIN32 )
		add_library( ${LIB_BWN} SHARED ${SRCS_BWN} ${MOC_SRCS_BWN} )
	endif( WIN32 )

	target_link_libraries( ${LIB_BWN} ${QT_LIBRARIES} )
	install( TARGETS ${LIB_BWN} DESTINATION lib )
endif ( BuildBaulkWindow )


##| BaulkXML
if ( BuildBaulkXML )
	QT4_WRAP_CPP( MOC_SRCS_BXML ${HDRS_BXML} )
	
	if ( WIN32 )
		add_library( ${LIB_BXML} STATIC ${SRCS_BXML} ${MOC_SRCS_BXML} )
	else ( WIN32 )
		add_library( ${LIB_BXML} SHARED ${SRCS_BXML} ${MOC_SRCS_BXML} )
	endif( WIN32 )
	
	target_link_libraries( ${LIB_BXML} ${QT_LIBRARIES} )
	install( TARGETS ${LIB_BXML} DESTINATION lib )
endif ( BuildBaulkXML )

