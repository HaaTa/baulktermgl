/*
    This file is part of Konsole, a terminal emulator for KDE.
    
    Copyright (C) 2006-7 by Robert Knight <robertknight@gmail.com>
    Copyright (C) 1997,1998 by Lars Doelle <lars.doelle@on-line.de>
    
    Rewritten for QT4 by e_k <e_k at users.sourceforge.net>, Copyright (C)2008
    Additional fixes for BaulkTerm by Jacob Alexander (HaaTa) <haata at users.sourceforge.net>, Copyright (C) 2008

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
    02110-1301  USA.
*/


// Qt Includes
#include <QtGui/QClipboard>
#include <QtGui/QScrollBar>
#include <QApplication>
#include <QTimer>

// Project Includes
#include "TerminalDisplay.h"

#include "Filter.h"
#include "ScreenWindow.h"
#include "ColorTables.h"


// Static Variables
// scroll increment used when dragging selection at top/bottom of window.

// static
bool TerminalDisplay::_antialiasText = true;
//bool TerminalDisplay::HAVE_TRANSPARENCY = false;
bool TerminalDisplay::HAVE_TRANSPARENCY = true;




/* ------------------------------------------------------------------------- */
/*                                                                           */
/*                         Constructor / Destructor                          */
/*                                                                           */
/* ------------------------------------------------------------------------- */

void TerminalDisplay::basicSetup( QWidget *dispWidget )
{
	// Initialize variables
	_screenWindow = 0;
	_allowBell = true;
	_fontHeight = 1;
	_fontWidth = 1;
	_fontAscent = 1;
	_lines = 1;
	_columns = 1;
	_usedLines = 1;
	_usedColumns = 1;
	_contentHeight = 1;
	_contentWidth = 1;
	_image = 0;
	_randomSeed = 0;
	_resizing = false;
	_terminalSizeHint = false;
	_terminalSizeStartup = true;
	_bidiEnabled = false;
	_actSel = 0;
	_wordSelectionMode = false;
	_lineSelectionMode = false;
	_preserveLineBreaks = false;
	_columnSelectionMode = false;
	_scrollbarLocation = NoScrollBar;
	_wordCharacters = ":@-./_~";
	_bellMode = SystemBeepBell;
	_blinking = false;
	_cursorBlinking = false;
	_hasBlinkingCursor = false;
	_ctrlDrag = false;
	_tripleClickMode = SelectWholeLine;
	_isFixedSize = false;
	_possibleTripleClick = false;
	_resizeWidget = 0;
	_resizeTimer = 0;
	_flowControlWarningEnabled = false;
	_outputSuspendedLabel = 0;
	_lineSpacing = 0;
	_colorsInverted = false;
	_blendColor = qRgba(0,0,0,0xff);
	_cursorShape = BlockCursor;


	displayWidget = dispWidget;
	eventObject = new TerminalDisplayEvent();

  // The offsets are not yet calculated.
  // Do not calculate these too often to be more smoothly when resizing
  // konsole in opaque mode.
  _topMargin = DEFAULT_TOP_MARGIN;
  _leftMargin = DEFAULT_LEFT_MARGIN;

  // create scroll bar for scrolling output up and down
  // set the scroll bar's slider to occupy the whole area of the scroll bar initially
/*
  _scrollBar = new QScrollBar( displayWidget );
  setScroll(0,0); 
  _scrollBar->setCursor( Qt::ArrowCursor );
  QObject::connect(_scrollBar, SIGNAL(valueChanged(int)), displayWidget, 
  					  SLOT(scrollBarPositionChanged(int)));
*/

  // setup timers for blinking cursor and text
  _blinkTimer   = new QTimer( displayWidget );
  QObject::connect(_blinkTimer, SIGNAL(timeout()), displayWidget, SLOT(blinkEvent()));
  _blinkCursorTimer   = new QTimer( displayWidget );
  QObject::connect(_blinkCursorTimer, SIGNAL(timeout()), displayWidget, SLOT(blinkCursorEvent()));

//  QCursor::setAutoHideCursor( this, true );
  
  setUsesMouse(true);
  setColorTable(whiteonblack_color_table); 
//  setColorTable(blackonlightyellow_color_table); 

  //set up a warning message when the user presses Ctrl+S to avoid confusion
  QObject::connect( eventObject, SIGNAL(flowControlKeyPressed(bool)), displayWidget, SLOT(outputSuspended(bool)) );
}

void TerminalDisplay::basicCleanup()
{
	delete eventObject;
}





/* ------------------------------------------------------------------------- */
/*                                                                           */
/*                             Misc Operations                               */
/*                                                                           */
/* ------------------------------------------------------------------------- */

void TerminalDisplay::setRandomSeed(uint randomSeed) { _randomSeed = randomSeed; }
uint TerminalDisplay::randomSeed() const { return _randomSeed; }





/* ------------------------------------------------------------------------- */
/*                                                                           */
/*                                Colors                                     */
/*                                                                           */
/* ------------------------------------------------------------------------- */

/* Note that we use ANSI color order (bgr), while IBMPC color order is (rgb)

   Code        0       1       2       3       4       5       6       7
   ----------- ------- ------- ------- ------- ------- ------- ------- -------
   ANSI  (bgr) Black   Red     Green   Yellow  Blue    Magenta Cyan    White
   IBMPC (rgb) Black   Blue    Green   Cyan    Red     Magenta Yellow  White
*/

ScreenWindow* TerminalDisplay::screenWindow() const
{
    return _screenWindow;
}

void TerminalDisplay::setScreenWindow(ScreenWindow* window)
{
    // disconnect existing screen window if any
    if ( _screenWindow )
    {
        QObject::disconnect( _screenWindow , 0 , widgetObj() , 0 );
    }

    _screenWindow = window;

    if ( window )
    {
//#warning "The order here is not specified - does it matter whether updateImage or updateLineProperties comes first?"
        QObject::connect( _screenWindow , SIGNAL(outputChanged()) , widgetObj() , SLOT(updateLineProperties()) );
        QObject::connect( _screenWindow , SIGNAL(outputChanged()) , widgetObj() , SLOT(updateImage()) );
	window->setWindowLines(_lines);
    }
}

const ColorEntry* TerminalDisplay::colorTable() const
{
  return _colorTable;
}

void TerminalDisplay::setColorTable(const ColorEntry table[])
{
	for (int i = 0; i < TABLE_COLORS; i++)
		_colorTable[i] = table[i];

	QPalette p = widgetObj()->palette();
	p.setColor( widgetObj()->backgroundRole(), _colorTable[DEFAULT_BACK_COLOR].color );
	widgetObj()->setPalette( p );

	// Avoid propagating the palette change to the scroll bar 
	//_scrollBar->setPalette( QApplication::palette() );  

	widgetObj()->update();
}

void TerminalDisplay::setColorTableEntry( int colorEntry, QColor color, bool transparency, bool bold ) 
{
  ColorEntry table[TABLE_COLORS] = _colorTable;
  table[colorEntry] = ColorEntry( color, transparency, bold );
  setColorTable( table );
}




/* ------------------------------------------------------------------------- */
/*                                                                           */
/*                             Display Operations                            */
/*                                                                           */
/* ------------------------------------------------------------------------- */

/**
 A table for emulating the simple (single width) unicode drawing chars.
 It represents the 250x - 257x glyphs. If it's zero, we can't use it.
 if it's not, it's encoded as follows: imagine a 5x5 grid where the points are numbered
 0 to 24 left to top, top to bottom. Each point is represented by the corresponding bit.

 Then, the pixels basically have the following interpretation:
 _|||_
 -...-
 -...-
 -...-
 _|||_

where _ = none
      | = vertical line.
      - = horizontal line.
 */

void TerminalDisplay::setKeyboardCursorColor(bool useForegroundColor, const QColor& color)
{
    if (useForegroundColor)
        _cursorColor = QColor(); // an invalid color means that
                                 // the foreground color of the
                                 // current character should
                                 // be used

    else
        _cursorColor = color;
}

void TerminalDisplay::setBlinkingCursor(bool blink)
{
  _hasBlinkingCursor=blink;
  
  if (blink && !_blinkCursorTimer->isActive()) 
      _blinkCursorTimer->start(BLINK_DELAY);
  
  if (!blink && _blinkCursorTimer->isActive()) 
  {
    _blinkCursorTimer->stop();
    if (_cursorBlinking)
      blinkCursorEvent();
    else
      _cursorBlinking = false;
  }
}





/* ------------------------------------------------------------------------- */
/*                                                                           */
/*                                Keyboard                                   */
/*                                                                           */
/* ------------------------------------------------------------------------- */



void TerminalDisplay::setLineSpacing(uint i)
{
  _lineSpacing = i;
  setVTFont( displayWidget->font() ); // Trigger an update.
}

// calculate the needed size
void TerminalDisplay::setSize(int columns, int lines)
{
  //FIXME - Not quite correct, a small amount of additional space
  // will be used for margins, the scrollbar etc.
  // we need to allow for this so that '_size' does allow
  // enough room for the specified number of columns and lines to fit

  QSize newSize = QSize( columns * _fontWidth  ,
				 lines * _fontHeight   );

  if ( newSize != displayWidget->size() )
  {
    _size = newSize;
    displayWidget->updateGeometry();
  }
}



/* ------------------------------------------------------------------------- */
/*                                                                           */
/*                                Scrollbar                                  */
/*                                                                           */
/* ------------------------------------------------------------------------- */




/* ------------------------------------------------------------------------- */
/*                                                                           */
/*                               Clipboard                                   */
/*                                                                           */
/* ------------------------------------------------------------------------- */


void TerminalDisplay::setSelection(const QString& t)
{
  QApplication::clipboard()->setText(t, QClipboard::Selection);
}


