/*
    Copyright (C) 2007 by Robert Knight <robertknight@gmail.com>
    Copyright (C) 1997,1998 by Lars Doelle <lars.doelle@on-line.de>

    Rewritten for QT4 by e_k <e_k at users.sourceforge.net>, Copyright (C)2008
    Additional fixes for BaulkTerm by Jacob Alexander (HaaTa) <haata at users.sourceforge.net>, Copyright (C) 2008

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
    02110-1301  USA.
*/

#ifndef TERMINALDISPLAY_H
#define TERMINALDISPLAY_H

// Qt
#include <QtGui/QColor>
#include <QtCore/QPointer>
#include <QObject>

// Konsole
#include "Filter.h"
#include "Character.h"
#include "ColorTables.h"
#include "TerminalDisplayEvent.h"


// Pre-Declarations
class QLabel;
class QScrollBar;
class TerminalDisplayEvent;


extern unsigned short vt100_graphics[32];

class ScreenWindow;

/**
 * A widget which displays output from a terminal emulation and sends input keypresses and mouse activity
 * to the terminal.
 *
 * When the terminal emulation receives new output from the program running in the terminal, 
 * it will update the display by calling updateImage().
 *
 * TODO More documentation
 */
class TerminalDisplay
{
public:
    /** Returns the terminal color palette used by the display. */
    const ColorEntry* colorTable() const;
    /** Sets the terminal color palette used by the display. */
    void setColorTable(const ColorEntry table[]);
    /** Sets an Entry in the ColorTable */
    void setColorTableEntry( int colorEntry, QColor color, bool transparency, bool bold );
    /**
     * Sets the seed used to generate random colors for the display
     * (in color schemes that support them).
     */
    void setRandomSeed(uint seed);
    /**
     * Returns the seed used to generate random colors for the display
     * (in color schemes that support them).
     */
    uint randomSeed() const;

    /** Sets the opacity of the terminal display. */
    virtual void setOpacity(qreal opacity) = 0;

    /** 
     * This enum describes the location where the scroll bar is positioned in the display widget.
     */
    enum ScrollBarPosition 
    { 
        /** Do not show the scroll bar. */
        NoScrollBar=0, 
        /** Show the scroll bar on the left side of the display. */
        ScrollBarLeft=1, 
        /** Show the scroll bar on the right side of the display. */
        ScrollBarRight=2 
    };
    /** 
     * Specifies whether the terminal display has a vertical scroll bar, and if so whether it
     * is shown on the left or right side of the display.
     */
    virtual void setScrollBarPosition(ScrollBarPosition position) = 0;

    /** 
     * Sets the current position and range of the display's scroll bar.
     *
     * @param cursor The position of the scroll bar's thumb.
     * @param lines The maximum value of the scroll bar.
     */
    virtual void setScroll(int cursor, int lines) = 0;

    /** Returns true if the cursor is set to blink or false otherwise. */
    bool blinkingCursor() { return _hasBlinkingCursor; }
    /** Specifies whether or not the cursor blinks. */
    virtual void setBlinkingCursor(bool blink);

    void setCtrlDrag(bool enable) { _ctrlDrag=enable; }
    bool ctrlDrag() { return _ctrlDrag; }

	/** 
     *  This enum describes the methods for selecting text when
 	 *  the user triple-clicks within the display. 
 	 */
	enum TripleClickMode
	{
		/** Select the whole line underneath the cursor. */
		SelectWholeLine,
		/** Select from the current cursor position to the end of the line. */
		SelectForwardsFromCursor
	};
    /** Sets how the text is selected when the user triple clicks within the display. */	
    void setTripleClickMode(TripleClickMode mode) { _tripleClickMode = mode; }
	/** See setTripleClickSelectionMode() */
    TripleClickMode tripleClickMode() { return _tripleClickMode; }

    virtual void setLineSpacing(uint);
    uint lineSpacing() const { return _lineSpacing; }

    virtual void emitSelection(bool useXselection,bool appendReturn) = 0;

    /**
     * This enum describes the available shapes for the keyboard cursor.
     * See setKeyboardCursorShape()
     */
    enum KeyboardCursorShape
    {
        /** A rectangular block which covers the entire area of the cursor character. */
        BlockCursor,
        /** 
         * A single flat line which occupies the space at the bottom of the cursor
         * character's area.
         */
        UnderlineCursor,
        /** 
         * An cursor shaped like the capital letter 'I', similar to the IBeam 
         * cursor used in Qt/KDE text editors.
         */
        IBeamCursor
    };
    /** 
     * Sets the shape of the keyboard cursor.  This is the cursor drawn   
     * at the position in the terminal where keyboard input will appear.
     *
     * In addition the terminal display widget also has a cursor for 
     * the mouse pointer, which can be set using the QWidget::setCursor()
     * method.
     *
     * Defaults to BlockCursor
     */
    void setKeyboardCursorShape(KeyboardCursorShape shape) { _cursorShape = shape; }
    /**
     * Returns the shape of the keyboard cursor.  See setKeyboardCursorShape()
     */
    KeyboardCursorShape keyboardCursorShape() const { return _cursorShape; }

    /**
     * Sets the color used to draw the keyboard cursor.  
     *
     * The keyboard cursor defaults to using the foreground color of the character
     * underneath it.
     *
     * @param useForegroundColor If true, the cursor color will change to match
     * the foreground color of the character underneath it as it is moved, in this
     * case, the @p color parameter is ignored and the color of the character
     * under the cursor is inverted to ensure that it is still readable.
     * @param color The color to use to draw the cursor.  This is only taken into
     * account if @p useForegroundColor is false.
     */
    virtual void setKeyboardCursorColor(bool useForegroundColor , const QColor& color);

    /** 
     * Returns the color of the keyboard cursor, or an invalid color if the keyboard
     * cursor color is set to change according to the foreground color of the character
     * underneath it. 
     */
    QColor keyboardCursorColor() const { return _cursorColor; }

    /**
     * Returns the number of lines of text which can be displayed in the widget.
     *
     * This will depend upon the height of the widget and the current font.
     * See fontHeight()
     */
    int  lines()   { return _lines;   }
    /**
     * Returns the number of characters of text which can be displayed on
     * each line in the widget.
     *
     * This will depend upon the width of the widget and the current font.
     * See fontWidth()
     */
    int  columns() { return _columns; }

    /**
     * Returns the height of the characters in the font used to draw the text in the display.
     */
    int  fontHeight()   { return _fontHeight;   }
    /**
     * Returns the width of the characters in the display.  
     * This assumes the use of a fixed-width font.
     */
    int  fontWidth()    { return _fontWidth; }

    virtual void setSize(int cols, int lins);
    virtual void setFixedSize(int cols, int lins) = 0;
    
    // reimplemented
    QSize sizeHintTerm() const { return _size; }

    /**
     * Sets which characters, in addition to letters and numbers, 
     * are regarded as being part of a word for the purposes
     * of selecting words in the display by double clicking on them.
     *
     * The word boundaries occur at the first and last characters which
     * are either a letter, number, or a character in @p wc
     *
     * @param wc An array of characters which are to be considered parts
     * of a word ( in addition to letters and numbers ).
     */
    void setWordCharacters(const QString& wc) { _wordCharacters = wc; }
    /** 
     * Returns the characters which are considered part of a word for the 
     * purpose of selecting words in the display with the mouse.
     *
     * @see setWordCharacters()
     */
    QString wordCharacters() { return _wordCharacters; }

    /** 
     * Sets the type of effect used to alert the user when a 'bell' occurs in the 
     * terminal session.
     *
     * The terminal session can trigger the bell effect by calling bell() with
     * the alert message.
     */
    virtual void setBellMode(int mode) { _bellMode = mode; }
    /** 
     * Returns the type of effect used to alert the user when a 'bell' occurs in
     * the terminal session.
     * 
     * See setBellMode()
     */
    int bellMode() { return _bellMode; }

    /**
     * This enum describes the different types of sounds and visual effects which
     * can be used to alert the user when a 'bell' occurs in the terminal
     * session.
     */
    enum BellMode
    { 
        /** A system beep. */
        SystemBeepBell=0, 
        /** 
         * KDE notification.  This may play a sound, show a passive popup
         * or perform some other action depending on the user's settings.
         */
        NotifyBell=1, 
        /** A silent, visual bell (eg. inverting the display's colors briefly) */
        VisualBell=2, 
        /** No bell effects */
        NoBell=3 
    };

    virtual void setSelection(const QString &t);

    virtual QFont getVTFont() = 0;
    virtual void setVTFont(const QFont& font) = 0;

    /**
     * Sets whether or not the current height and width of the 
     * terminal in lines and columns is displayed whilst the widget
     * is being resized.
     */
    void setTerminalSizeHint(bool on) { _terminalSizeHint=on; }
    /** 
     * Returns whether or not the current height and width of
     * the terminal in lines and columns is displayed whilst the widget
     * is being resized.
     */
    bool terminalSizeHint() { return _terminalSizeHint; }
    /** 
     * Sets whether the terminal size display is shown briefly
     * after the widget is first shown.
     *
     * See setTerminalSizeHint() , isTerminalSizeHint()
     */
    void setTerminalSizeStartup(bool on) { _terminalSizeStartup=on; }

    void setBidiEnabled(bool set) { _bidiEnabled=set; }
    bool isBidiEnabled() { return _bidiEnabled; }

    /**
     * Sets the terminal screen section which is displayed in this widget.
     * When updateImage() is called, the display fetches the latest character image from the
     * the associated terminal screen window.
     *
     * In terms of the model-view paradigm, the ScreenWindow is the model which is rendered
     * by the TerminalDisplay.
     */
    virtual void setScreenWindow( ScreenWindow* window );
    /** Returns the terminal screen section which is displayed in this widget.  See setScreenWindow() */
    ScreenWindow* screenWindow() const;

    static bool HAVE_TRANSPARENCY;

	// Pass-through functions for QWidget
	bool isHidden() const { return displayWidget->isHidden(); }
	void resize( const QSize &size ) { displayWidget->resize( size ); }
	QWidget *widgetObj() { return displayWidget; }
	TerminalDisplayEvent *eventObj() { return eventObject; }

public slots:

    /** 
     * Causes the terminal display to fetch the latest character image from the associated
     * terminal screen ( see setScreenWindow() ) and redraw the display.
     */
    virtual void updateImage() = 0; 
    /**
     * Causes the terminal display to fetch the latest line status flags from the 
     * associated terminal screen ( see setScreenWindow() ).  
     */ 
    virtual void updateLineProperties() = 0;

    /** Copies the selected text to the clipboard. */
    virtual void copyClipboard() = 0;
    /** 
     * Pastes the content of the clipboard into the 
     * display.
     */
    virtual void pasteClipboard() = 0;
    /**
     * Pastes the content of the selection into the
     * display.
     */
    virtual void pasteSelection() = 0;

	/** 
 	  * Changes whether the flow control warning box should be shown when the flow control
 	  * stop key (Ctrl+S) are pressed.
 	  */
	virtual void setFlowControlWarningEnabled(bool enabled) = 0;
	
    /** 
	 * Causes the widget to display or hide a message informing the user that terminal
	 * output has been suspended (by using the flow control key combination Ctrl+S)
	 *
	 * @param suspended True if terminal output has been suspended and the warning message should
	 *				 	be shown or false to indicate that terminal output has been resumed and that
	 *				 	the warning message should disappear.
	 */ 
	virtual void outputSuspended(bool suspended) = 0;

    /**
     * Sets whether the program whoose output is being displayed in the view
     * is interested in mouse events.
     *
     * If this is set to true, mouse signals will be emitted by the view when the user clicks, drags
     * or otherwise moves the mouse inside the view.
     * The user interaction needed to create selections will also change, and the user will be required
     * to hold down the shift key to create a selection or perform other mouse activities inside the 
     * view area - since the program running in the terminal is being allowed to handle normal mouse
     * events itself.
     *
     * @param usesMouse Set to true if the program running in the terminal is interested in mouse events
     * or false otherwise.
     */
    virtual void setUsesMouse(bool usesMouse) = 0;
  
    /** See setUsesMouse() */
    virtual bool usesMouse() const = 0;

    /** 
     * Shows a notification that a bell event has occurred in the terminal.
     * TODO: More documentation here
     */
    virtual void bell(const QString& message) = 0;


protected:
	// Initial setup, run these in constructor and destructor of inherited classes
	void basicSetup( QWidget *displayWidget );
	void basicCleanup();

	QWidget *displayWidget;
	TerminalDisplayEvent *eventObject;

	// the window onto the terminal screen which this display
	// is currently showing.  
	QPointer<ScreenWindow> _screenWindow;

	bool _allowBell;

	bool _fixedFont;      // has fixed pitch
	int  _fontHeight;     // height
	int  _fontWidth;      // width
	int  _fontAscent;     // ascend

	int _leftMargin;    // offset
	int _topMargin;    // offset

	int _lines;      // the number of lines that can be displayed in the widget
	int _columns;    // the number of columns that can be displayed in the widget

	int _usedLines;  // the number of lines that are actually being used, this will be less
	                 // than 'lines' if the character image provided with setImage() is smaller
	                 // than the maximum image size which can be displayed

	int _usedColumns; // the number of columns that are actually being used, this will be less
	                  // than 'columns' if the character image provided with setImage() is smaller
	                  // than the maximum image size which can be displayed

	int _contentHeight;
	int _contentWidth;
	Character* _image; // [lines][columns]
	                   // only the area [usedLines][usedColumns] in the image contains valid data

	int _imageSize;
	QVector<LineProperty> _lineProperties;

	ColorEntry _colorTable[TABLE_COLORS];
	uint _randomSeed;

	bool _resizing;
	bool _terminalSizeHint;
	bool _terminalSizeStartup;
	bool _bidiEnabled;
	bool _mouseMarks;

	QPoint  _iPntSel;        // initial selection point
	QPoint  _pntSel;         // current selection point
	QPoint  _tripleSelBegin; // help avoid flicker
	int     _actSel;         // selection state
	bool    _wordSelectionMode;
	bool    _lineSelectionMode;
	bool    _preserveLineBreaks;
	bool    _columnSelectionMode;

	QClipboard*  _clipboard;
	QScrollBar* _scrollBar;
	ScrollBarPosition _scrollbarLocation;
	QString     _wordCharacters;
	int         _bellMode;

	bool _blinking;            // hide text in paintEvent
	bool _hasBlinker;          // has characters to blink
	bool _cursorBlinking;      // hide cursor in paintEvent
	bool _hasBlinkingCursor;   // has blinking cursor enabled
	bool _ctrlDrag;            // require Ctrl key for drag
	TripleClickMode _tripleClickMode;
	bool _isFixedSize;         // Columns / lines are locked.
	QTimer* _blinkTimer;       // active when hasBlinker
	QTimer* _blinkCursorTimer; // active when hasBlinkingCursor

	QString _dropText;
	int _dndFileCount;

	bool _possibleTripleClick;  // is set in mouseDoubleClickEvent and deleted
	                            // after QApplication::doubleClickInterval() delay


	QLabel* _resizeWidget;
	QTimer* _resizeTimer;

	bool _flowControlWarningEnabled;

	//widgets related to the warning message that appears when the user presses Ctrl+S to suspend
	//terminal output - informing them what has happened and how to resume output
	QLabel* _outputSuspendedLabel; 

	uint _lineSpacing;

	bool _colorsInverted; // true during visual bell

	QSize _size;

	QRgb _blendColor;

	QRect _mouseOverHotspotArea;

	KeyboardCursorShape _cursorShape;

	// custom cursor color.  if this is invalid then the foreground
	// color of the character under the cursor is used
	QColor _cursorColor;  


	struct InputMethodData
	{
	QString preeditString;
	QRect previousPreeditRect;
	};
	InputMethodData _inputMethodData;

	static bool _antialiasText;   // do we antialias or not

	//the delay in milliseconds between redrawing blinking text
	static const int BLINK_DELAY = 500;
	static const int DEFAULT_LEFT_MARGIN = 1;
	static const int DEFAULT_TOP_MARGIN = 1;

protected slots:
	virtual void blinkCursorEvent() = 0;
};

#endif // TERMINALDISPLAY_H

