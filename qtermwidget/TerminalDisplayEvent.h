// Jacob Alexander 2012

#ifndef TERMINALDISPLAYEVENT_H
#define TERMINALDISPLAYEVENT_H

// Qt Includes
#include <QObject>
#include <QKeyEvent>

// Pre-Declarations
class TerminalDisplay;

class TerminalDisplayEvent : public QObject {
	Q_OBJECT
public:
	TerminalDisplayEvent() {}

	void keyPressedSignalEvent( QKeyEvent *e ) { emit keyPressedSignal( e ); }
	void flowControlKeyPressedEvent( bool suspend ) { emit flowControlKeyPressed( suspend ); }
	void mouseSignalEvent(int button, int column, int line, int eventType) { emit mouseSignal( button, column, line, eventType ); }
	void changedFontMetricSignalEvent(int height, int width) { emit changedFontMetricSignal( height, width ); }
	void changedContentSizeSignalEvent(int height, int width) { emit changedContentSizeSignal( height, width ); }
	void configureRequestEvent( TerminalDisplay* td, int state, const QPoint& position ) { emit configureRequest( td, state, position ); }
	void rightClickActionEvent() { emit rightClickAction(); }
	void isBusySelectingEvent(bool sel) { emit isBusySelecting( sel ); }
	void sendStringToEmuEvent(const char* c) { emit sendStringToEmu( c ); }

signals:

	/**
	 * Emitted when the user presses a key whilst the terminal widget has focus.
	 */
	void keyPressedSignal(QKeyEvent *e);

	/**
	 * Emitted when the user presses the suspend or resume flow control key combinations 
	 * 
	 * @param suspend true if the user pressed Ctrl+S (the suspend output key combination) or
	 * false if the user pressed Ctrl+Q (the resume output key combination)
	 */
	void flowControlKeyPressed(bool suspend);

	/** 
	 * A mouse event occurred.
	 * @param button The mouse button (0 for left button, 1 for middle button, 2 for right button, 3 for release)
	 * @param column The character column where the event occurred
	 * @param line The character row where the event occurred
	 * @param eventType The type of event.  0 for a mouse press / release or 1 for mouse motion
	 */
	void mouseSignal(int button, int column, int line, int eventType);
	void changedFontMetricSignal(int height, int width);
	void changedContentSizeSignal(int height, int width);

	/** 
	 * Emitted when the user right clicks on the display, or right-clicks with the Shift
	 * key held down if usesMouse() is true.
	 *
	 * This can be used to display a context menu.
	 */
	void configureRequest( TerminalDisplay*, int state, const QPoint& position );
	void rightClickAction();

	void isBusySelecting(bool);
	void sendStringToEmu(const char*);
};

#endif

