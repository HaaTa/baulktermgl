// Jacob Alexander 2012

#ifndef TERMINALDISPLAY2D_H
#define TERMINALDISPLAY2D_H

// Qt
#include <QtGui/QWidget>

// Project Includes
#include "TerminalDisplay.h"


// Pre-Declarations
class QDrag;
class QDragEnterEvent;
class QDropEvent;
class QTimer;
class QEvent;
class QFrame;
class QGridLayout;
class QKeyEvent;
class QShowEvent;
class QHideEvent;
class QWidget;


class TerminalDisplay2D : public QWidget, public TerminalDisplay
{
	Q_OBJECT

public:
	/** Constructs a new terminal display widget with the specified parent. */
	TerminalDisplay2D( QWidget *parent = 0 );
	~TerminalDisplay2D();

	// TODO
	QFont getVTFont() { return font(); }

    /** 
     * Reimplemented.  Has no effect.  Use setVTFont() to change the font
     * used to draw characters in the display.
     */
    virtual void setFont(const QFont &);

    /** 
     * Sets the font used to draw the display.  Has no effect if @p font
     * is larger than the size of the display itself.    
     */
    virtual void setVTFont(const QFont& font);

    /** Sets the opacity of the terminal display. */
    void setOpacity(qreal opacity);

    virtual void setFixedSize(int cols, int lins);
    virtual void emitSelection(bool useXselection,bool appendReturn);
    virtual void setScroll(int cursor, int lines);
    virtual void setScrollBarPosition(ScrollBarPosition position);

public slots:
	/** 
	 * Causes the terminal display to fetch the latest character image from the associated
	 * terminal screen ( see setScreenWindow() ) and redraw the display.
	 */
	virtual void updateImage(); 
	virtual void bell(const QString& message);
	virtual void outputSuspended(bool suspended);
	virtual void updateLineProperties();
	virtual void setUsesMouse(bool usesMouse);
	virtual void copyClipboard();
	virtual void pasteClipboard();
	virtual void pasteSelection();
	virtual void setFlowControlWarningEnabled(bool enabled);
	virtual bool usesMouse() const { return _mouseMarks; }

protected:
	virtual bool event( QEvent * );

	virtual void paintEvent( QPaintEvent * );

	virtual void showEvent(QShowEvent*);
	virtual void hideEvent(QHideEvent*);
	virtual void resizeEvent(QResizeEvent*);

	virtual void fontChange(const QFont &font);

	virtual void keyPressEvent(QKeyEvent* event);
	virtual void mouseDoubleClickEvent(QMouseEvent* ev);
	virtual void mousePressEvent( QMouseEvent* );
	virtual void mouseReleaseEvent( QMouseEvent* );
	virtual void mouseMoveEvent( QMouseEvent* );
	virtual void extendSelection( const QPoint& pos );
	virtual void wheelEvent( QWheelEvent* );

	virtual bool focusNextPrevChild( bool next );


	virtual int charClass(quint16) const;

	void clearImage();

	void mouseTripleClickEvent(QMouseEvent* ev);

	// reimplemented
	virtual void inputMethodEvent ( QInputMethodEvent* event );
	virtual QVariant inputMethodQuery( Qt::InputMethodQuery query ) const;

	static void setTransparencyEnabled(bool enable)
	{
		HAVE_TRANSPARENCY = enable;
	}


protected slots:

	void scrollBarPositionChanged(int value);
	void blinkEvent();
	void blinkCursorEvent();

	//Renables bell noises and visuals.  Used to disable further bells for a short period of time
	//after emitting the first in a sequence of bell events.
	void enableBell() { _allowBell = true; }

private slots:

	void swapColorTable();
	void tripleClickTimeout();  // resets possibleTripleClick

private:

	// -- Drawing helpers --

	// divides the part of the display specified by 'rect' into
	// fragments according to their colors and styles and calls
	// drawTextFragment() to draw the fragments 
	void drawContents(QPainter &paint, const QRect &rect);
	// draws a section of text, all the text in this section
	// has a common color and style
	void drawTextFragment(QPainter& painter, const QRect& rect, 
		  const QString& text, const Character* style); 
	// draws the background for a text fragment
	// if useOpacitySetting is true then the color's alpha value will be set to
	// the display's transparency (set with setOpacity()), otherwise the background
	// will be drawn fully opaque
	void drawBackground(QPainter& painter, const QRect& rect, const QColor& color,
					bool useOpacitySetting);
	// draws the cursor character
	void drawCursor(QPainter& painter, const QRect& rect , const QColor& foregroundColor, 
			       const QColor& backgroundColor , bool& invertColors);
	// draws the characters or line graphics in a text fragment
	void drawCharacters(QPainter& painter, const QRect& rect,  const QString& text, 
				   const Character* style, bool invertCharacterColor);
	// draws a string of line graphics
	void drawLineCharString(QPainter& painter, int x, int y, 
		    const QString& str, const Character* attributes);

	// draws the preedit string for input methods
	void drawInputMethodPreeditString(QPainter& painter , const QRect& rect);

	// --

	// maps an area in the character image to an area on the widget 
	QRect imageToWidget(const QRect& imageArea) const;

	// maps a point on the widget to the position ( ie. line and column ) 
	// of the character at that point.
	void getCharacterPosition(const QPoint& widgetPoint,int& line,int& column) const;

	// the area where the preedit string for input methods will be draw
	QRect preeditRect() const;

	// shows a notification window in the middle of the widget indicating the terminal's
	// current size in columns and lines
	void showResizeNotification();

	// scrolls the image by a number of lines.  
	// 'lines' may be positive ( to scroll the image down ) 
	// or negative ( to scroll the image up )
	// 'region' is the part of the image to scroll - currently only
	// the top, bottom and height of 'region' are taken into account,
	// the left and right are ignored.
	void scrollImage(int lines , const QRect& region);

	void calcGeometry();
	void propagateSize();
	void updateImageSize();
	void makeImage();

	// returns the position of the cursor in columns and lines
	QPoint cursorPosition() const;


	// Variables
	QGridLayout* _gridLayout;

	// TODO
};

#endif

