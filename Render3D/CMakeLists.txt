###| CMAKE Render3D |###

### TODO Add proper detection for GNU Scientific Library

#| Project
project( Render3D )
cmake_minimum_required( VERSION 2.6 )
set( HEAD_DIR ${PROJECT_SOURCE_DIR}/.. )

#| Sources
set( SRCS
	TerminalDisplay3D.cpp
	glwidget.cpp
	MBuffer.cpp
	MLine.cpp
	MChar.cpp
	MCharCache.cpp
	MParticleSystem.cpp
	MPixel.cpp
	MSpline.cpp
	Material.cpp

	EffectsDialog.cpp
)

#| Headers (if a class is derived from QObject and is in a header, it must be included here)
set( HDRS
	glwidget.h
	TerminalDisplay3D.h

	EffectsDialog.h
)

#| XML UI Files
set( UIS
	EffectsDialog.ui
)

#| Binary
set( LIB
	Render3D
)


#| Qt4 Setup
add_definitions( 
	-Wall -ggdb
)
find_package( OpenGL REQUIRED )
find_package( Qt4 COMPONENTS QtCore QtGui QtOpenGL REQUIRED )
include( ${QT_USE_FILE} )
QT4_WRAP_CPP( MOC_SRCS ${HDRS} )
QT4_WRAP_UI( UI_HDRS ${UIS} )

include_directories ( ${CMAKE_BINARY_DIR}/Render3D ${PROJECT_SOURCE_DIR} ${HEAD_DIR}/qtermwidget )

#| Create the Library
add_library ( ${LIB} SHARED ${SRCS} ${MOC_SRCS} ${UI_HDRS} )

#| Link Needed Libraries
target_link_libraries( ${LIB}
	${QT_LIBRARIES}
	${OPENGL_LIBRARIES}
	gsl
	gslcblas
)

#| Installation
install( TARGETS ${LIB} DESTINATION lib	)

