// Jacob Alexander 2012

// Qt Includes
#include <QGLShader>
#include <QtGui>
#include <QtOpenGL>

// System Includes
#include <math.h>

// OpenGL Includes
#include <GL/glu.h>

// Project Includes
#include <glwidget.h>


// Defines
#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif


// Post/Pre ***************************************************************************************
GLWidget::GLWidget( QWidget *parent ) : QGLWidget( QGLFormat( QGL::SampleBuffers ), parent )
{
	// Initialize Variables
	mouseMode = MOUSE_Terminal;
	projMode  = PROJ_Orthographic;

	fontRenderType = MCharCache::RENDER_Texture;

	skyboxEnabled = false;
	skyboxImagePath = "";
	skyboxLoaded = false;
	reflectionEnabled = false;

	volumeShadowsEnabled = false;

	glowEnabled = false;
	motionBlurEnabled = false;

	// Receives updates from wheelEvent (in TerminalDisplay3D), and is used by motion blur
	wheelVelocityCounter = 0;

	mouseLBut_enabled = false;
	mouseMBut_enabled = false;
	mouseRBut_enabled = false;

	mouseXCurrent  = 0;
	mouseYCurrent  = 0;
	mouseZCurrent  = 0;

	mouseZoomCurrent = 0;
	//cameraZoom = 0.104; // Seems to be a reasonable starting point XXX for glFrustum
	cameraZoom = -300;

	cameraXMovement = 0;
	cameraYMovement = 0;
	cameraZMovement = 0;

	cameraXRotation = 0;
	cameraYRotation = 0;
	cameraZRotation = 0;


	// Setup character cache
	QFont mfont = QApplication::font();
	mfont.setFamily("Terminus");
	//mfont.setPointSize( 8 );
	mfont.setPointSize( 12 );
	mfont.setStyleHint( QFont::TypeWriter );
	mcharcache.setFont( mfont );


	// Setup character buffer
	mbuffer.setLineSpacing( 0 ); // This is the visible space between character lines (not affected by the character)
	mbuffer.setCharHeight( mcharcache.getFontHeight() );
	mbuffer.setCharWidth( mcharcache.getFontWidth() );


	// Setup Render Scheduler
	currentRenderInstance = 0;
	prevRenderInstance = 0;

	reRenderOnUpdate = false;

	renderInstanceCounter = new QTimer( this );
	renderInstanceCounter->setSingleShot( false );
	connect( renderInstanceCounter, SIGNAL( timeout() ), this, SLOT( updateTimeInstance() ) );
	//renderInstanceCounter->start( 17 ); // 58.823 FPS
	//renderInstanceCounter->start( 50 ); // 20 FPS
	renderInstanceCounter->start( 200 ); // Slow

	particleAnimationEnabled = false;
	keyframeAnimationEnabled = false;
	textPathAnimationEnabled = false;


	// Shader Control Variable Initialization
	currentShader = 0;

	/*
	Character a((quint16)'A');
	Character b((quint16)'B');
	Character c((quint16)'c');
	Character d((quint16)'d');
	Character e((quint16)'1');
	Character f((quint16)'@');
	Character g((quint16)'*');
	Character h((quint16)'y');
	Character i((quint16)'g');

	MLine lineTest;
	lineTest.addChar( mcharcache.requestChar( &a, MCharCache::RENDER_Pixel )->copy( &a ) );
	qDebug() <<"BLA";
	lineTest.addChar( mcharcache.requestChar( &b, MCharCache::RENDER_Pixel )->copy( &b ) );
	lineTest.addChar( mcharcache.requestChar( &c, MCharCache::RENDER_Pixel )->copy( &c ) );
	lineTest.addChar( mcharcache.requestChar( &d, MCharCache::RENDER_Pixel )->copy( &d ) );
	lineTest.addChar( mcharcache.requestChar( &e, MCharCache::RENDER_Pixel )->copy( &e ) );
	lineTest.addChar( mcharcache.requestChar( &f, MCharCache::RENDER_Pixel )->copy( &f ) );
	lineTest.addChar( mcharcache.requestChar( &g, MCharCache::RENDER_Pixel )->copy( &g ) );
	lineTest.addChar( mcharcache.requestChar( &h, MCharCache::RENDER_Pixel )->copy( &h ) );
	lineTest.addChar( mcharcache.requestChar( &i, MCharCache::RENDER_Pixel )->copy( &i ) );
	mbuffer.addLine( lineTest );
	*/
	/*
	MLine lineTest2;
	lineTest2.addChar( mcharcache.requestChar( (quint16)'A', 0, MCharCache::RENDER_Texture )->copy() );
	lineTest2.addChar( mcharcache.requestChar( (quint16)'B', 0, MCharCache::RENDER_Texture )->copy() );
	lineTest2.addChar( mcharcache.requestChar( (quint16)'c', 0, MCharCache::RENDER_Texture )->copy() );
	lineTest2.addChar( mcharcache.requestChar( (quint16)'d', 0, MCharCache::RENDER_Texture )->copy() );
	lineTest2.addChar( mcharcache.requestChar( (quint16)'1', 0, MCharCache::RENDER_Texture )->copy() );
	lineTest2.addChar( mcharcache.requestChar( (quint16)'@', 0, MCharCache::RENDER_Texture )->copy() );
	lineTest2.addChar( mcharcache.requestChar( (quint16)'*', 0, MCharCache::RENDER_Texture )->copy() );
	lineTest2.addChar( mcharcache.requestChar( (quint16)'y', 0, MCharCache::RENDER_Texture )->copy() );
	lineTest2.addChar( mcharcache.requestChar( (quint16)'g', 0, MCharCache::RENDER_Texture )->copy() );
	mbuffer.addLine( lineTest2 );
	*/
}

GLWidget::~GLWidget()
{
	// Cleanup shaders
	cleanupShaders();
}


// Functions **************************************************************************************
QSize GLWidget::minimumSizeHint() const
{
	return QSize( 5, 5 );
}

QSize GLWidget::sizeHint() const
{
	return QSize( 400, 400 );
}


// GL Function ************************************************************************************
void GLWidget::initializeGL()
{
	// Enable XXX NAMES TODO
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	glShadeModel(GL_SMOOTH);

	blurFramebufferHoriz  = 0;
	blurFramebufferVert   = 0;
	blurFramebufferMotion = 0;
	reflectionFramebuffer = 0;

	// Load in shaders
	loadShaders();
}

void GLWidget::paintGL()
{
	// Adjust Projection Matrix
	glMatrixMode( GL_PROJECTION );

	// Move the camera
	adjustProjection( width(), height() );
	adjustCameraTransformation();

	// Add the light source
	// TODO

	// Model View Mode (now that the camera has been adjusted)
	glMatrixMode( GL_MODELVIEW );

	// Clear buffers
	// TODO <- Does anything else need to get cleared?
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();

	// Render Scene into first framebuffer
	if ( glowEnabled )
	{
		blurFramebufferHoriz->bind();
	}
	else if ( motionBlurEnabled )
	{
		blurFramebufferMotion->bind();
	}

	// Relflection framebuffer
	if ( reflectionEnabled && skyboxEnabled )
		reflectionFramebuffer->bind();

	// Render Skybox if enabled
	if ( skyboxEnabled )
	{
		// Load Skybox Textures
		if ( !skyboxLoaded )
			loadSkyboxTexture();

		renderSkybox();
	}
	// Render Black Box
	else
	{
		renderSkybox( true );
	}


	// Apply shaders used for vertex rendering of the characters
	basicShader->bind();

	// Pass time instance to shader
	basicShader->setUniformValue( "timeInstance", abs((int)currentRenderInstance) );

	// Pass animation step to shader
	basicShader->setUniformValue( "animationStep", 6 );
	basicShader->setUniformValue( "animationOn", keyframeAnimationEnabled );

	// Render MBuffer from given start to end location
	// Usually this is from end to height of the window (number of rows + row offset)
	// In the scroll case, the position is arbitrary
	mbuffer.renderMBuffer( renderLineCurrent, renderLines, this, basicShader );


	// Volume Shadow Processing

	// Release the vertex rendering shaders
	basicShader->release();


	// Reflections enabled
	if ( reflectionEnabled && skyboxEnabled )
	{
		reflectionFramebuffer->release();
		reflectionShader->bind();
		reflectionShader->setUniformValue( "inputTextureFront", 1 );

		QMatrix4x4 viewMatrix;
		viewMatrix.translate( cameraXMovement, cameraYMovement, cameraZMovement + cameraZoom );
		viewMatrix.rotate( cameraXRotation, 1, 0, 0 );
		viewMatrix.rotate( cameraYRotation, 0, 1, 0 );
		viewMatrix.rotate( cameraZRotation, 0, 0, 1 );
		QMatrix4x4 viewMatrixInverse;
		viewMatrixInverse.translate( 2500, 0, 2500 );


		reflectionShader->setUniformValue( "viewMatrix", viewMatrix );
		reflectionShader->setUniformValue( "viewMatrixInverse",viewMatrixInverse  );
		glActiveTexture( GL_TEXTURE1 );
		glBindTexture( GL_TEXTURE_2D, skyboxTextures[1] );
		glActiveTexture( GL_TEXTURE0 );
		renderFramebuffer( reflectionFramebuffer, 0, reflectionShader );
		reflectionShader->release();
	}


	// Go through queue of particle system, rendering each of them
	if ( particleAnimationEnabled ) for ( int c = 0; c < particleSystems.size(); c++ )
	{
		int timeElapsed = currentRenderInstance - prevRenderInstance;
		particleSystems[c].updateSystem( timeElapsed );

		// TODO cleanup old systems
		particleSystems[c].renderMParticleSystem( this );
	}


	// Glow Effects Enabled
	if ( glowEnabled )
	{
		// Release first blur framebuffer
		blurFramebufferHoriz->release();


		blurFramebufferVert->bind();
		horizGlowShader->bind();
		horizGlowShader->setUniformValue( "targetWidth", (float)blurFramebufferHoriz->size().width() );
		renderFramebuffer( blurFramebufferHoriz, 0, horizGlowShader );
		horizGlowShader->release();
		blurFramebufferVert->release();


		// Add Motion Blur to pipeline if enabled
		if ( motionBlurEnabled ) blurFramebufferMotion->bind();


		// Do vertical blur
		vertGlowShader->bind();
		vertGlowShader->setUniformValue( "targetHeight", (float)blurFramebufferVert->size().height() );
		renderFramebuffer( blurFramebufferVert, blurFramebufferHoriz, vertGlowShader );
		vertGlowShader->release();
	}

	// Motion Blur Enabled
	if ( motionBlurEnabled )
	{
		blurFramebufferMotion->release();
		vertMotionShader->bind();
		vertMotionShader->setUniformValue( "velocity", (float)wheelVelocityCounter );
		vertMotionShader->setUniformValue( "targetHeight", (float)blurFramebufferVert->size().height() );
		renderFramebuffer( blurFramebufferMotion, 0, vertMotionShader );
		vertMotionShader->release();
		if ( wheelVelocityCounter != 0 )
			reRenderOnUpdate = true; // So the blur will be cleared on stop
	}


	// Clear velocity counter, regardless of whether it was used or not
	wheelVelocityCounter = 0;


	// Check render buffer to see if an update needs to be scheduled
	//if ( ( mbuffer.doParticleAnimation() && particleAnimationEnabled )
	//  || ( mbuffer.doKeyframeAnimation() && keyframeAnimationEnabled ) )
	if ( keyframeAnimationEnabled || particleAnimationEnabled )
	//if ( mbuffer.doKeyframeAnimation() && keyframeAnimationEnabled )
	{
		reRenderOnUpdate = true;
	}
}

void GLWidget::resizeGL( int width, int height )
{
	glViewport( 0, 0, width, height );

	// Re-create framebuffers for new viewport size
	updateFramebufferObjects( width, height );

	// Send new width and height to Screen (terminal interface) to recompute view
	//  Assume exact pixel mapping to the Screen Window at all times
	resizeTerminalScreen( width, height );

	// TODO <- Perhaps this render sequence should be delayed while the line flushes are done?
}

void GLWidget::adjustProjection( int width, int height )
{
	glLoadIdentity();

	// TODO near, far <- are these values ok?
	double near = 0.1;
	double far  = 10000.0;

	// Perspective parameters
	double zoom = cameraZoom;
	double fovy = 120;

	// Depending on the projection mode, adjust
	switch ( projMode )
	{
	// Build the orthgraphic projection
	case PROJ_Orthographic:
		gluOrtho2D( 0, width, height, 0 );
		break;

	// Build the perspective projection
	case PROJ_Perspective:
		gluPerspective( fovy, (double)width / (double)height, near, far );
		//glFrustum( -zoom * width, zoom * width, -zoom * height, zoom * height, near, far ); // Doesn't handle rotations
		//glTranslated( 0,0,-350 ); // XXX Arbitrary 1 to 1 pixel rendering...
		//glTranslated( 0,0,-336 ); // XXX Arbitrary 1 to 1 pixel rendering...
		glTranslated( 0,0,zoom ); // XXX Arbitrary 1 to 1 pixel rendering...
		// XXX Not perfect, but gets position in the top left
		glTranslated( width / -2, height / 2 + mcharcache.getFontHeight(), 0 );
		glRotated( 180, 0.0, 1.0, 0.0 ); // Rotate the character - Y direction (flipped by default)
		glRotated( 180, 0.0, 0.0, 1.0 ); // Rotate the character - Z direction (flipped by default)
		break;

	default:
		qWarning() << "Invalid projection mode";
		break;
	}
}

void GLWidget::adjustCameraTransformation()
{
	// Translate camera
	glTranslated( cameraXMovement, cameraYMovement, cameraZMovement );

	// Rotate camera
	glRotated( cameraXRotation, 1.0, 0.0, 0.0 );
	glRotated( cameraYRotation, 0.0, 1.0, 0.0 );
	glRotated( cameraZRotation, 0.0, 0.0, 1.0 );
}

void GLWidget::renderSkybox( bool blackBox )
{
	// TODO selectable skybox

	// Store the current render matrix
	glPushMatrix();

	// Settings for skybox
	double x,y,z;
	double width, height, length;

	//x = -2500;
	x = -3500;
	y = -3500;
	z = -5000;
	width  = 8000;
	height = 8000;
	length = 8000;

	// Load Perspective
	glLoadIdentity();
	gluLookAt( x + width / 2, y + height / 2, z + length / 2, // Position
		cameraXMovement + 1200, cameraYMovement + 300, cameraZMovement + 9200, // Camera Rotation
		0,1,0 );

	// Skybox
	if ( !blackBox )
	{
		// Enable needed gl features
		glPushAttrib( GL_ENABLE_BIT );
		glEnable( GL_TEXTURE_2D );
		glDisable( GL_DEPTH_TEST );
		glDisable( GL_LIGHTING );
		glDisable( GL_BLEND );

		// Set the quads to white in case of alpha skybox image and clipping
		glColor4f( 1,1,1,1 );
	}
	// Black Box
	else
	{
		glColor4f( 0,0,0,1 );
	}

	// Base rotation for skybox
	glRotated( 180, 1.0, 0.0, 0.0 ); // X
	glRotated(  90, 0.0, 1.0, 0.0 ); // Y

	// Back quad for skybox
	if ( !blackBox ) glBindTexture( GL_TEXTURE_2D, skyboxTextures[0] );
	glBegin(GL_QUADS);
	glTexCoord2d( 1.0, 0.0 ); glVertex3d( x + width, y,          z );
	glTexCoord2d( 1.0, 1.0 ); glVertex3d( x + width, y + height, z );
	glTexCoord2d( 0.0, 1.0 ); glVertex3d( x,         y + height, z );
	glTexCoord2d( 0.0, 0.0 ); glVertex3d( x,         y,          z );
	glEnd();

	// Front quad
	if ( !blackBox ) glBindTexture( GL_TEXTURE_2D, skyboxTextures[1] );
	glBegin(GL_QUADS);
	glTexCoord2d( 1.0, 0.0 ); glVertex3d( x,         y,          z + length );
	glTexCoord2d( 1.0, 1.0 ); glVertex3d( x,         y + height, z + length );
	glTexCoord2d( 0.0, 1.0 ); glVertex3d( x + width, y + height, z + length );
	glTexCoord2d( 0.0, 0.0 ); glVertex3d( x + width, y,          z + length );
	glEnd();

	// Down quad (bottom)
	if ( !blackBox ) glBindTexture( GL_TEXTURE_2D, skyboxTextures[2] );
	glBegin(GL_QUADS);
	glTexCoord2d( 1.0, 0.0 ); glVertex3d( x,         y,          z );
	glTexCoord2d( 1.0, 1.0 ); glVertex3d( x,         y,          z + length );
	glTexCoord2d( 0.0, 1.0 ); glVertex3d( x + width, y,          z + length );
	glTexCoord2d( 0.0, 0.0 ); glVertex3d( x + width, y,          z );
	glEnd();

	// Up quad (top)
	if ( !blackBox ) glBindTexture( GL_TEXTURE_2D, skyboxTextures[3] );
	glBegin(GL_QUADS);
	glTexCoord2d( 0.0, 0.0 ); glVertex3d( x + width, y + height, z );
	glTexCoord2d( 1.0, 0.0 ); glVertex3d( x + width, y + height, z + length );
	glTexCoord2d( 1.0, 1.0 ); glVertex3d( x,         y + height, z + length );
	glTexCoord2d( 0.0, 1.0 ); glVertex3d( x,         y + height, z );
	glEnd();

	// Left quad
	if ( !blackBox ) glBindTexture( GL_TEXTURE_2D, skyboxTextures[4] );
	glBegin(GL_QUADS);
	glTexCoord2d( 1.0, 1.0 ); glVertex3d( x,         y + height, z );
	glTexCoord2d( 0.0, 1.0 ); glVertex3d( x,         y + height, z + length );
	glTexCoord2d( 0.0, 0.0 ); glVertex3d( x,         y,          z + length );
	glTexCoord2d( 1.0, 0.0 ); glVertex3d( x,         y,          z ); 
	glEnd();

	// Right quad
	if ( !blackBox ) glBindTexture( GL_TEXTURE_2D, skyboxTextures[5] );
	glBegin(GL_QUADS);
	glTexCoord2d( 0.0, 0.0 ); glVertex3d( x + width, y,          z );
	glTexCoord2d( 1.0, 0.0 ); glVertex3d( x + width, y,          z + length );
	glTexCoord2d( 1.0, 1.0 ); glVertex3d( x + width, y + height, z + length );
	glTexCoord2d( 0.0, 1.0 ); glVertex3d( x + width, y + height, z );
	glEnd();

	// Restore old matrix and attributes
	if ( !blackBox ) glPopAttrib();
	glPopMatrix();
}

void GLWidget::loadSkyboxTexture()
{
	// TODO make selectable
	QString skyback  = "FullMoonBack2048.png";
	QString skydown  = "FullMoonDown2048.png";
	QString skyfront = "FullMoonFront2048.png";
	QString skyleft  = "FullMoonLeft2048.png";
	QString skyright = "FullMoonRight2048.png";
	QString skyup    = "FullMoonUp2048.png";
	QString path = "../FullMoon/";

	// TODO make sure image exists

	QImage skyTextureBack ( path + skyback );
	QImage skyTextureDown ( path + skydown );
	QImage skyTextureFront( path + skyfront );
	QImage skyTextureLeft ( path + skyleft );
	QImage skyTextureRight( path + skyright );
	QImage skyTextureUp   ( path + skyup );

	glGenTextures( 6, &skyboxTextures[0] );

	skyboxTextures[0] = bindTexture( skyTextureBack );
	skyboxTextures[1] = bindTexture( skyTextureFront );
	skyboxTextures[2] = bindTexture( skyTextureDown );
	skyboxTextures[3] = bindTexture( skyTextureUp );
	skyboxTextures[4] = bindTexture( skyTextureLeft );
	skyboxTextures[5] = bindTexture( skyTextureRight );

	skyboxLoaded = true;
}


// Shader Functions *******************************************************************************
void GLWidget::loadShaders()
{
	// Basic Shader Program
	loadShader( &basicShader,
		&keyframeVertexShader,
		&basicFragmentShader,
		"keyframe", "basic" );


	// Horizontal Gaussian Bleed Protection Glow Shader
	loadShader( &horizGlowShader,
		&basicVertexShader1,
		&horizGlowFragmentShader,
		"basic", "horizGaussianBleedGlow" );


	// Vertical Gaussian Additive Glow Shader
	loadShader( &vertGlowShader,
		&basicVertexShader2,
		&vertGlowFragmentShader,
		"basic", "vertGaussianAdditiveGlow" );


	// Vertical Gaussian Veloctiy Shader
	loadShader( &vertMotionShader,
		&basicVertexShader3,
		&vertMotionFragmentShader,
		"basic", "vertGaussianVelocityBlur" );


	// Reflection Shader
	loadShader( &reflectionShader,
		&reflectionVertexShader,
		&reflectionFragmentShader,
		"reflection", "reflection" );
}

void GLWidget::loadShader( QGLShaderProgram **program, QGLShader **vertex, QGLShader **fragment, QString vertexName, QString fragmentName )
{
	// TODO Do proper file check/existance, maybe dynamic shader additions, bla, bla, bla
	QString path = "Shaders/";


	// Creat Shader Program
	*program = new QGLShaderProgram;


	// Attempt to build Vertex Shader
	*vertex = new QGLShader( QGLShader::Vertex );
	if ( (*vertex)->compileSourceFile( path + vertexName + ".vertex" ) )
	{
		(*program)->addShader( *vertex );
	}
	else qCritical() << vertexName << "Vertex Shader Error:" << (*vertex)->log();


	// Attempt to build Fragment Shader
	*fragment = new QGLShader( QGLShader::Fragment );
	if ( (*fragment)->compileSourceFile( path + fragmentName + ".fragment" ) )
	{
		(*program)->addShader( *fragment );
	}
	else qCritical() << fragmentName << "Fragment Shader Error:" << (*fragment)->log();


	// Attempt to link vertex and fragment shaders
	if ( !(*program)->link() )
		qCritical() << vertexName << "+" << fragmentName
			<< "Shader Linker Error:" << (*program)->log();
}

void GLWidget::cleanupShaders()
{
	delete basicShader;
	delete horizGlowShader;
	delete vertGlowShader;
	delete basicVertexShader1;
	delete basicVertexShader2;
	delete keyframeVertexShader;
	delete basicFragmentShader;
	delete horizGlowFragmentShader;
	delete vertGlowFragmentShader;
}


// Framebuffer Functions **************************************************************************
void GLWidget::updateFramebufferObjects( int width, int height )
{
	// Cleanup previous framebuffers if necessary
	if ( blurFramebufferHoriz != 0 )
		delete blurFramebufferHoriz;
	if ( blurFramebufferVert != 0 )
		delete blurFramebufferVert;
	if ( blurFramebufferMotion != 0 )
		delete blurFramebufferMotion;
	if ( reflectionFramebuffer != 0 )
		delete reflectionFramebuffer;

	blurFramebufferHoriz  = new QGLFramebufferObject( width, height );
	blurFramebufferVert   = new QGLFramebufferObject( width, height );
	blurFramebufferMotion = new QGLFramebufferObject( width, height );
	reflectionFramebuffer = new QGLFramebufferObject( width, height );
}

void GLWidget::cleanupFramebufferObjects()
{
	delete blurFramebufferHoriz;
	delete blurFramebufferVert;
	delete blurFramebufferMotion;
	delete reflectionFramebuffer;
}

void GLWidget::renderFramebuffer( QGLFramebufferObject *fbo, QGLFramebufferObject *fbo2, QGLShaderProgram *shader )
{
	// If shader is being used, set the necessary uniforms
	if ( shader != 0 )
	{
		shader->setUniformValue( "inputTexture", 0 );
	}

	const GLfloat quadVertices[] = {
		-1.0, -1.0, 0,
		 1.0, -1.0, 0,
		 1.0,  1.0, 0,
		-1.0,  1.0, 0
	};
	const GLfloat texCoords[] = {
		 0.0,  0.0,
		 1.0,  0.0,
		 1.0,  1.0,
		 0.0,  1.0
	};

	glActiveTexture( GL_TEXTURE0 );
	glEnable( GL_TEXTURE_2D );
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glBindTexture( GL_TEXTURE_2D, fbo->texture() );

	if ( fbo2 != 0 )
	{
		shader->setUniformValue( "inputTextureOrig", 1 );
		glActiveTexture( GL_TEXTURE1 );
		glBindTexture( GL_TEXTURE_2D, fbo2->texture() );
		glActiveTexture( GL_TEXTURE0 );
	}

	glColor3f(1.0f,1.0f,1.0f); // White (debug)
	glPushMatrix();
	glLoadIdentity();
	glMatrixMode( GL_PROJECTION );
	glPushMatrix();
	glLoadIdentity();

	// Draw textured quad, default, no shader
	if ( shader == 0 )
	{
		glEnableClientState( GL_VERTEX_ARRAY );
		glEnableClientState( GL_TEXTURE_COORD_ARRAY );
		glVertexPointer( 3, GL_FLOAT, 0, quadVertices );
		glTexCoordPointer( 2, GL_FLOAT, 0, texCoords );
		glDrawArrays( GL_QUADS, 0, 4 );
		glDisableClientState( GL_VERTEX_ARRAY );
		glDisableClientState( GL_TEXTURE_COORD_ARRAY );
	}
	// Draw textured quad, using shader
	else
	{
		// Setup vertex attribute
		shader->setAttributeArray(
			"inputVertex",
			GL_FLOAT,
			quadVertices,
			3,
			0 );
		shader->enableAttributeArray("inputVertex");

		// Setup texture coord attribute
		shader->setAttributeArray(
			"inputTexCoord",
			GL_FLOAT,
			texCoords,
			2,
			0 );
		shader->enableAttributeArray("inputTexCoord");

		// Draw textured quad
		glDrawArrays( GL_QUADS, 0, 4 );

		shader->disableAttributeArray("inputVertex");
		shader->disableAttributeArray("inputTexCoord");
	}

	glPopMatrix();
	glMatrixMode( GL_MODELVIEW );
	glPopMatrix();
	glDisable( GL_TEXTURE_2D );
}


// Event Functions ********************************************************************************
void GLWidget::mousePressEvent( QMouseEvent *event )
{
	// Enable given mouse buttons
	switch ( event->button() )
	{
	case Qt::LeftButton:
		mouseLBut_enabled = true;
		break;

	case Qt::MiddleButton:
		mouseMBut_enabled = true;
		break;

	case Qt::RightButton:
		mouseRBut_enabled = true;
		break;

	default:
		qWarning() << "Unknown mouse button press";
		break;
	}

	event->accept();
}

void GLWidget::mouseReleaseEvent( QMouseEvent *event )
{
	// Disable given mouse button
	switch ( event->button() )
	{
	case Qt::LeftButton:
		mouseLBut_enabled = false;
		break;

	case Qt::MiddleButton:
		mouseMBut_enabled = false;
		break;

	case Qt::RightButton:
		mouseRBut_enabled = false;
		break;

	default:
		qWarning() << "Unknown mouse button release";
		break;
	}

	event->accept();
}

void GLWidget::mouseMoveEvent( QMouseEvent *event )
{
	// Determine the mouse movement mode
	switch ( mouseMode )
	{
	case MOUSE_Terminal:
		// TODO
		break;

	case MOUSE_Translation:
		// Send commands to camera translation movement
		mouseCameraTranslationControl( event );
		event->accept();
		break;

	case MOUSE_Rotation:
		// Send commands to camera rotation movement
		mouseCameraRotationControl( event );
		event->accept();
		break;

	default:
		qWarning() << "Unknown mouse movement mode";
		break;
	}
}


// Mouse Control **********************************************************************************
void GLWidget::mouseCameraTranslationControl( QMouseEvent *event )
{
#define TRANSLATION_MULT      0.5
//#define TRANSLATION_MULT_ZOOM 0.001 // glFrustum adjust
#define TRANSLATION_MULT_ZOOM 1.0 // gluPerspective adjust

	// For each of the axes calculate the different since the last coord update
	// Then signal that much change, and update the x coord for the next refresh

	if ( mouseLBut_enabled )
	{
		if ( mouseXCurrent != 0 )
		{
			cameraXMovement += ( event->x() - mouseXCurrent ) * TRANSLATION_MULT;
			cameraYMovement += ( event->y() - mouseYCurrent ) * TRANSLATION_MULT;
			qDebug() << "Movement (X,Y): " << cameraXMovement << "," << cameraYMovement;
			updateGL();
		}
		mouseXCurrent = event->x();
		mouseYCurrent = event->y();
	}
	else
	{
		mouseXCurrent = 0;
		mouseYCurrent = 0;
	}

	if ( mouseMBut_enabled )
	{
		if ( mouseZoomCurrent != 0 )
		{
			cameraZoom += ( event->y() - mouseZoomCurrent  ) * TRANSLATION_MULT_ZOOM;
			qDebug() << "Movement (Zoom): " << cameraZoom;
			updateGL();
		}
		mouseZoomCurrent = event->y();
	}
	else
	{
		mouseZoomCurrent = 0;
	}

	if ( mouseRBut_enabled )
	{
		if ( mouseZCurrent != 0 )
		{
			cameraZMovement += ( event->y() - mouseZCurrent  ) * TRANSLATION_MULT;
			qDebug() << "Movement (Z): " << cameraZMovement;
			updateGL();
		}
		mouseZCurrent  = event->y();
	}
	else
	{
		mouseZCurrent  = 0;
	}
}

void GLWidget::mouseCameraRotationControl( QMouseEvent *event )
{
#define ROTATION_MULT  0.5

	// For each of the rotation axes calculate the different since the last coord update
	// Then signal that much change, and update the x coord for the next refresh

	if ( mouseLBut_enabled )
	{
		if ( mouseXCurrent != 0 )
		{
			cameraXRotation += ( event->x() - mouseXCurrent ) * ROTATION_MULT;
			cameraYRotation += ( event->y() - mouseYCurrent ) * ROTATION_MULT;
			qDebug() << "Rotation (X,Y): " << cameraXRotation << "," << cameraYRotation;
			updateGL();
		}
		mouseXCurrent = event->x();
		mouseYCurrent = event->y();
	}
	else
	{
		mouseXCurrent = 0;
		mouseYCurrent = 0;
	}

	if ( mouseMBut_enabled )
	{
	}
	else
	{
	}

	if ( mouseRBut_enabled )
	{
		if ( mouseZCurrent != 0 )
		{
			cameraZRotation += ( event->y() - mouseZCurrent ) * ROTATION_MULT;
			qDebug() << "Rotation (Z): " << cameraZRotation;
			updateGL();
		}
		mouseZCurrent = event->y();
	}
	else
	{
		mouseZCurrent  = 0;
	}
}


// Public Slots ***********************************************************************************
void GLWidget::setTerminalMouseMode()
{
	mouseMode = MOUSE_Terminal;
}

void GLWidget::setTranslationMouseMode()
{
	mouseMode = MOUSE_Translation;
}

void GLWidget::setRotationMouseMode()
{
	mouseMode = MOUSE_Rotation;
}

void GLWidget::setOrthographicProjectionMode()
{
	projMode = PROJ_Orthographic;
	updateGL();
}

void GLWidget::setPerspectiveProjectionMode()
{
	projMode = PROJ_Perspective;
	updateGL();
}

void GLWidget::setParticleAnimation()
{
	particleAnimationEnabled = !particleAnimationEnabled;
	updateGL();
}

void GLWidget::setKeyframeAnimation()
{
	keyframeAnimationEnabled = !keyframeAnimationEnabled;
	updateGL();
}

void GLWidget::setTextPathAnimation()
{
	textPathAnimationEnabled = !textPathAnimationEnabled;
	mbuffer.setLinePathSpline( textPathAnimationEnabled );
	updateGL();
}

void GLWidget::setMotionBlur()
{
	motionBlurEnabled = !motionBlurEnabled;
	updateGL();
}

void GLWidget::setVolumeShadows()
{
	volumeShadowsEnabled = !volumeShadowsEnabled;
	updateGL();
}

void GLWidget::setReflection()
{
	reflectionEnabled = !reflectionEnabled;
	updateGL();
}

void GLWidget::setSkybox()
{
	skyboxEnabled = !skyboxEnabled;
	updateGL();
}

void GLWidget::setGlow()
{
	glowEnabled = !glowEnabled;
	updateGL();
}

void GLWidget::setTextureFont()
{
	fontRenderType = MCharCache::RENDER_Texture;
	//mcharcache.clearCache();
	updateGL();
}

void GLWidget::setPixelFont()
{
	fontRenderType = MCharCache::RENDER_Pixel;
	//mcharcache.clearCache();
	updateGL();
}


// Private Slots **********************************************************************************
void GLWidget::updateTimeInstance()
{
	// TODO Overflow case not handled, may cause glitch at that instance

	// Increment the instance
	currentRenderInstance++;

	// Check for scheduled animations
	if ( reRenderOnUpdate )
	{
		reRenderOnUpdate = false;

		updateGL();
	}
}

