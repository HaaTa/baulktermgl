// Jacob Alexander 2012

// Qt Includes
#include <QGLWidget>

// OpenGL Includes
#include <GL/glu.h>

// Project Includes
#include <MChar.h>



// Post/Pre ***************************************************************************************
MChar::MChar()
{
	dirty = false;
	copiedCharacter = false;
	creationInstance = 0;
	animationID = 0;
}

MChar::~MChar()
{
}


/**************************************************************************************************
 *
 *                                       MCharPixel
 *
 **************************************************************************************************/
MCharPixel::MCharPixel()
{
	dirty = false;
	copiedCharacter = false;
	creationInstance = 0;
	animationID = 0;
}

MCharPixel::~MCharPixel()
{
	if ( copiedCharacter )
		delete character;
}


// Copy Functions *********************************************************************************
MChar *MCharPixel::copy( Character *characterStyled )
{
	// Copy this object
	MCharPixel *newObj = new MCharPixel();
	*newObj = *this;

	// Copy the given character (needed for the dirty mask)
	Character *copyChar = new Character;
	*copyChar = *characterStyled;
	newObj->setCharacter( copyChar );
	copiedCharacter = true;

	// Force re-calculation of vertices
	dirty = true;

	return newObj;
}


// Render Functions ******************************************************************************
void MCharPixel::renderMChar( QGLWidget *renderer, QGLBuffer *buffer, int offset, int index )
{
	// Initially no animations scheduled
	particleAnimation = false;
	keyframeAnimation = false;

	int newOffset = offset;
	int pos = 0;

	for ( int x = 0; x < size.width(); x++ )
	{
		for ( int y = 0; y < size.height(); y++ )
		{

			// Only add forground pixels to VBO
			//if ( pixels[pos].getType() == MPixel::FOREGROUND )
			{
				pixels[pos].renderMPixel( renderer, buffer, newOffset, x, y );
				GLfloat tmp[4*6*3];
				buffer->read( newOffset, &tmp[0], 4*6*3 );
//				for ( int c = 0; c <= 4*6*3; c+= 6 )
//					qDebug()<<tmp[c]<<tmp[c+1]<<tmp[c+2]<<tmp[c+3]<<tmp[c+4]<<tmp[c+5];
				newOffset += 4 * 6 * 3 * sizeof( GLfloat );
			}
			pos++;
		}
		pos++;
	}
	/*
	// Calculate the vertices of each of the pixels into the VBO
	for ( int c = 0; c < pixels.size(); c++ )
	{
		pixels[c].renderMPixel( renderer, buffer, newOffset, xPos, yPos );

		// Setup for the next pixel
		xPos++;
		newOffset += pixelVertices * 3 * sizeof( GLfloat );
		if ( xPos - index >= size.width() ) // Next row of MChar
		{
			xPos = index;
			yPos++;
		}
	}
	*/
	/*
	// Apply current transformation to model state before rendering each of the pixels
	glPushMatrix();
	glMultMatrixd( transform.constData() ); // XXX This will NOT work on ARM!

	// Render each of the pixels
	for ( int c = 0; c < pixels.size(); c++ )
		pixels[c].renderMPixel( renderer );

	// Unapply current transformation
	glPopMatrix();
	*/
}


// Functions **************************************************************************************
void MCharPixel::setColors( const Material &foreground, const Material &background )
{
	// Set the foreground and background materials
	this->foreground = foreground;
	this->background = background;

	/*
	// Set the pointers in each foreground pixel to this material
	for ( int c = 0; c < pixels.size(); c++ )
	{
		if ( pixels[c].getType() == MPixel::FOREGROUND )
			pixels[c].setMaterial( foreground );
		if ( pixels[c].getType() == MPixel::BACKGROUND )
			pixels[c].setMaterial( background );
	}
	*/
}

void MCharPixel::setWidth( const size_t &width )
{
	// Calculate height of character (pixels ARE truncated here if too many are added)
	size = QSize( width, pixels.size() / width );
}

void MCharPixel::addPixel( const MPixel &pixel )
{
	pixels.push_back( pixel );
}


/**************************************************************************************************
 *
 *                                       MCharTexture
 *
 **************************************************************************************************/
MCharTexture::MCharTexture()
{
	dirty = false;
	copiedCharacter = false;
	creationInstance = 0;
	animationID = 0;

	textures[0] = 0;
	lastRenderer = 0;

	// Set the default colours - TODO This should be referenced from MCharCache in some way
	foreground = Material( QColor( 0,0,0,255 ) ); 
	background = Material( QColor( 255,255,255,255 ) );
}

MCharTexture::~MCharTexture()
{
	// Cleanup character instance
	if ( copiedCharacter )
		delete character;

	// Cleanup texture
	if ( lastRenderer != 0 && textures[0] != 0 )
	{
		lastRenderer->deleteTexture( textures[0] );
		textures[0] = 0;
	}
}


// Copy Functions *********************************************************************************
MChar *MCharTexture::copy( Character *characterStyled )
{
	// Copy this object
	MCharTexture *newObj = new MCharTexture();
	*newObj = *this;

	// Copy the given character (needed for the dirty mask)
	Character *copyChar = new Character;
	*copyChar = *characterStyled;
	newObj->setCharacter( copyChar );
	copiedCharacter = true;

	// Force re-calculation of vertices
	dirty = true;

	return newObj;
}


// Render Functions *******************************************************************************
void MCharTexture::renderMChar( QGLWidget *renderer, QGLBuffer *buffer, int offset, int index )
{
	// Initially no animations scheduled
	particleAnimation = false;
	keyframeAnimation = false;

	// Load the texture if it hasn't been already
	//  or reload if changed
	if ( textures[0] == 0 )
	{
		textures[0] = renderer->bindTexture( texture,
		                                     GL_TEXTURE_2D,
		                                     GL_RGBA,
		                                     QGLContext::PremultipliedAlphaBindOption );
	}

	// Last Renderer
	lastRenderer = renderer;

	// Calculate the vertices
	const int parametersPerVertex = 8;
	const int totalVertices = 24;
	GLfloat charVertices[totalVertices * parametersPerVertex];
	determineVertices( &charVertices[0], index );

	// Add each of the vertices to the VBO
	buffer->write( offset, &charVertices[0], totalVertices * parametersPerVertex * sizeof( GLfloat ) );

	/* VBO Debug code
	GLfloat tmp[totalVertices * parametersPerVertex];
	buffer->read( offset, &tmp[0], totalVertices * parametersPerVertex * sizeof( GLfloat ) );
	for ( int c = 0; c < totalVertices * parametersPerVertex; c+= 8 )
qDebug()<< c/8 << ":"<<charVertices[c]<<charVertices[c+1]<<charVertices[c+2]<<charVertices[c+3]<<charVertices[c+4]<<charVertices[c+5]<<charVertices[c+6]<<charVertices[c+7]
     << " ---- " <<tmp[c]<<tmp[c+1]<<tmp[c+2]<<tmp[c+3]<<tmp[c+4]<<tmp[c+5]<<tmp[c+6]<<tmp[c+7];
	qDebug() << "DONE";
	*/
}

void MCharTexture::determineVertices( GLfloat *vr, int index )
{
	// Render the background cube (height x width x 1) with texture
	double height = (double)size.height();
	double width  = (double)size.width();

	// Offset, due to the index position
	double offset = index * width;

	// Bit cast of timeInstance
	GLfloat tIns = (float)abs((int)creationInstance);
	tIns = 0.0f;

	// Animation ID
	// TODO make configurable
	GLfloat aID = (float)animationID;
	aID = 1.0f;
	//qDebug() << aID << tIns;


	// (x;y;z;) : (2D tex cord) : (time instance;vertex id;animation id)
	// Front face - Actually reversed face, so image is flipped (mirrored)
	*vr++ = width + offset; *vr++ = height; *vr++ = 1.0; *vr++ = 1.0; *vr++ = 1.0; *vr++ = tIns; *vr++ = 7.0; *vr++ = aID;
	*vr++ =   0.0 + offset; *vr++ = height; *vr++ = 1.0; *vr++ = 0.0; *vr++ = 1.0; *vr++ = tIns; *vr++ = 8.0; *vr++ = aID;
	*vr++ =   0.0 + offset; *vr++ = 0.0;    *vr++ = 1.0; *vr++ = 0.0; *vr++ = 0.0; *vr++ = tIns; *vr++ = 5.0; *vr++ = aID;
	*vr++ = width + offset; *vr++ = 0.0;    *vr++ = 1.0; *vr++ = 1.0; *vr++ = 0.0; *vr++ = tIns; *vr++ = 6.0; *vr++ = aID;
	// Top face
	*vr++ = width + offset; *vr++ = height; *vr++ = 0.0; *vr++ = 1.0; *vr++ = 0.0; *vr++ = tIns; *vr++ = 3.0; *vr++ = aID;
	*vr++ =   0.0 + offset; *vr++ = height; *vr++ = 0.0; *vr++ = 1.0; *vr++ = 0.0; *vr++ = tIns; *vr++ = 4.0; *vr++ = aID;
	*vr++ =   0.0 + offset; *vr++ = height; *vr++ = 1.0; *vr++ = 1.0; *vr++ = 0.0; *vr++ = tIns; *vr++ = 8.0; *vr++ = aID;
	*vr++ = width + offset; *vr++ = height; *vr++ = 1.0; *vr++ = 1.0; *vr++ = 0.0; *vr++ = tIns; *vr++ = 7.0; *vr++ = aID;

	// Right face
	*vr++ = width + offset; *vr++ = height; *vr++ = 0.0; *vr++ = 1.0; *vr++ = 0.0; *vr++ = tIns; *vr++ = 3.0; *vr++ = aID;
	*vr++ = width + offset; *vr++ = height; *vr++ = 1.0; *vr++ = 1.0; *vr++ = 0.0; *vr++ = tIns; *vr++ = 7.0; *vr++ = aID;
	*vr++ = width + offset; *vr++ = 0.0;    *vr++ = 1.0; *vr++ = 1.0; *vr++ = 0.0; *vr++ = tIns; *vr++ = 6.0; *vr++ = aID;
	*vr++ = width + offset; *vr++ = 0.0;    *vr++ = 0.0; *vr++ = 1.0; *vr++ = 0.0; *vr++ = tIns; *vr++ = 2.0; *vr++ = aID;

	// Left face
	*vr++ =   0.0 + offset; *vr++ = height; *vr++ = 1.0; *vr++ = 1.0; *vr++ = 0.0; *vr++ = tIns; *vr++ = 8.0; *vr++ = aID;
	*vr++ =   0.0 + offset; *vr++ = height; *vr++ = 0.0; *vr++ = 1.0; *vr++ = 0.0; *vr++ = tIns; *vr++ = 4.0; *vr++ = aID;
	*vr++ =   0.0 + offset; *vr++ = 0.0;    *vr++ = 0.0; *vr++ = 1.0; *vr++ = 0.0; *vr++ = tIns; *vr++ = 1.0; *vr++ = aID;
	*vr++ =   0.0 + offset; *vr++ = 0.0;    *vr++ = 1.0; *vr++ = 1.0; *vr++ = 0.0; *vr++ = tIns; *vr++ = 5.0; *vr++ = aID;

	// Bottom face
	*vr++ =   0.0 + offset; *vr++ = 0.0;    *vr++ = 0.0; *vr++ = 1.0; *vr++ = 0.0; *vr++ = tIns; *vr++ = 1.0; *vr++ = aID;
	*vr++ = width + offset; *vr++ = 0.0;    *vr++ = 0.0; *vr++ = 1.0; *vr++ = 0.0; *vr++ = tIns; *vr++ = 2.0; *vr++ = aID;
	*vr++ = width + offset; *vr++ = 0.0;    *vr++ = 1.0; *vr++ = 1.0; *vr++ = 0.0; *vr++ = tIns; *vr++ = 6.0; *vr++ = aID;
	*vr++ =   0.0 + offset; *vr++ = 0.0;    *vr++ = 1.0; *vr++ = 1.0; *vr++ = 0.0; *vr++ = tIns; *vr++ = 5.0; *vr++ = aID;

	// Back face - Actually the viewed face due to camera adjustments
	*vr++ =   0.0 + offset; *vr++ = height; *vr++ = 0.0; *vr++ = 0.0; *vr++ = 1.0; *vr++ = tIns; *vr++ = 4.0; *vr++ = aID;
	*vr++ = width + offset; *vr++ = height; *vr++ = 0.0; *vr++ = 1.0; *vr++ = 1.0; *vr++ = tIns; *vr++ = 3.0; *vr++ = aID;
	*vr++ = width + offset; *vr++ = 0.0;    *vr++ = 0.0; *vr++ = 1.0; *vr++ = 0.0; *vr++ = tIns; *vr++ = 2.0; *vr++ = aID;
	*vr++ =   0.0 + offset; *vr++ = 0.0;    *vr++ = 0.0; *vr++ = 0.0; *vr++ = 0.0; *vr++ = tIns; *vr++ = 1.0; *vr++ = aID;
	// XXX OFFSET BUG HERE vertex id and animation id not getting mapped correctly through the VBO
	// Most likely an alignment somewhere...
}

void MCharTexture::bindTexture()
{
	// Offset the enumeration
	GLenum texUnit = GL_TEXTURE0;

	// Set the active texture unit
	glActiveTexture( texUnit );

	// Bind this MChar's texture to the texture unit
	glBindTexture( GL_TEXTURE_2D, textures[0] );
}


// Functions **************************************************************************************
void MCharTexture::setColors( const Material &foreground, const Material &background )
{
	// Look for old foreground and background colours
	QRgb oldFore = this->foreground.getColor().rgba();
	QRgb newFore =       foreground.getColor().rgba();
	QRgb oldBack = this->background.getColor().rgba();
	QRgb newBack =       background.getColor().rgba();

	//qDebug() << "OldF" << this->foreground.getColor() << "NewF" << foreground.getColor() << "OldB" << this->background.getColor() << "NewB" << background.getColor();

	// Fill Foreground colour
	for ( int line = 0; line < texture.height(); line++ )
	{
		QRgb *thisLine = (QRgb*)texture.scanLine( line );
		for ( int pixel = 0; pixel < texture.width(); pixel++ )
		{
			if ( thisLine[pixel] == oldFore )
			{
				thisLine[pixel] = newFore;
			}
			else if ( thisLine[pixel] == oldBack )
			{
				thisLine[pixel] = newBack;
			}
			else
			{
				qWarning() << "Invalid Color Set to Character Texture";
			}
		}
	}

	// Set the foreground and background materials
	this->foreground = foreground;
	this->background = background;

	// Cleanup the previous texture
	if ( lastRenderer != 0 && textures[0] != 0 )
	{
		lastRenderer->deleteTexture( textures[0] );
		textures[0] = 0;
	}
}

void MCharTexture::setWidth( const size_t &width )
{
	size.setWidth( width );
}

void MCharTexture::setHeight( const size_t &height )
{
	size.setHeight( height );
}

void MCharTexture::setTexture( QImage &image )
{
	texture = image;
}

