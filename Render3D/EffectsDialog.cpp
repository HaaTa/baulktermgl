// Jacob Alexander 2012

// Qt Includes

// OpenGL Includes

// Project Includes
#include <EffectsDialog.h>
#include "ui_EffectsDialog.h"


// Post/Pre ***************************************************************************************
EffectsDialog::EffectsDialog( QWidget *parent ) : QDialog( parent ), ui( new Ui::Dialog )
{
	// Setup Qt UI XML file
	ui->setupUi( this );
}

EffectsDialog::~EffectsDialog()
{
	// Cleanup
	delete ui;
}


// Functions **************************************************************************************

