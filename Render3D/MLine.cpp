// Jacob Alexander 2012

// Qt Includes
#include <QGLShaderProgram>
#include <QGLWidget>

// OpenGL Includes
#include <GL/glu.h>

// Project Includes
#include <MLine.h>


// Post/Pre ***************************************************************************************
MLine::MLine()
{
	prevLineRenderLength = 0;

	// Initialize VBO
	// Prepare the Vertex buffer to be created
	lineVertexBuffer = QGLBuffer( QGLBuffer::VertexBuffer );

	dirty = false;

	totalPixels = 0;
}

MLine::~MLine()
{
	// Flush, and remove the VBO
	if ( lineVertexBuffer.isCreated() )
		lineVertexBuffer.destroy();
}


// Render Functions *******************************************************************************
void MLine::prepareMLine( QGLWidget *renderer )
{
	// Initially no animations scheduled
	particleAnimation = false;
	keyframeAnimation = false;

	// Make sure there are actually characters to process
	if ( chars.size() <= 0 )
		return;

	// Get width, height of character
	QSize charSize = chars[0]->getSize();

	// Check if line is now larger than before
	// If so, flush the VBO, and create a new one
	if ( chars.size() > prevLineRenderLength )
	{
		// Create the buffer if it hasn't been already
		if ( !lineVertexBuffer.isCreated() && !lineVertexBuffer.create() )
			qCritical() << "Failed to create VBO...";

		// Bind to the current OpenGL context
		if ( !lineVertexBuffer.bind() )
			qCritical() << "Could not bind to OpenGL context while attempting to allocate VBO...";

		// Set the appropriate usage hint
		lineVertexBuffer.setUsagePattern( QGLBuffer::DynamicDraw );

		// Allocate the needed amount of space
		//int totalVertices = 4 * 6 * 3 * chars.size() * charSize.width() * charSize.height(); // (MCharPixel)
		int totalVertices = 4 * 6 * 8 * chars.size(); // TODO account for MCharPixel (MCharTexture)

		lineVertexBuffer.allocate( totalVertices * sizeof( GLfloat ) );

		// Determine number of texture units available for binding textures
		glGetIntegerv( GL_MAX_TEXTURE_IMAGE_UNITS, &textureUnits );
	}
	// Become one with the VBO (i.e. bind)
	else
	{
		lineVertexBuffer.bind();
	}

	// Prepare each render for MChar
	for ( int c = 0; c < chars.size(); c++ )
	{
		// Check if the character is "dirty", i.e. needs to be re-written to the vbo
		// This could be due to an animation, or newly placed MChar that needs to be generated
		if ( !chars[c]->isDirty() )
			continue;

		//int offset = c * 4 * 6 * 3 * sizeof( GLfloat ) * charSize.width() * charSize.height(); // (MCharPixel)
		int offset = c * 4 * 6 * 8 * sizeof( GLfloat ); // TODO account for spurious MCharPixel (MCharTexture)
		chars[c]->renderMChar( renderer, &lineVertexBuffer, offset, c );

		// Check the character to see if any animations need to be scheduled
		particleAnimation = particleAnimation || chars[c]->doParticleAnimation();
		keyframeAnimation = keyframeAnimation || chars[c]->doKeyframeAnimation();
	}

	// Release the VBO
	lineVertexBuffer.release();

	// Re-render, no longer dirty TODO animation check
	dirty = false;
	prevLineRenderLength = chars.size();
}

void MLine::renderMLine( QGLWidget *renderer, QGLShaderProgram *shader )
{
	// Make sure there are actually characters to process
	if ( chars.size() <= 0 )
		return;

	// Get width, height of character
	QSize charSize = chars[0]->getSize();

	// Apply line transform to chars in the line
	glPushMatrix();
	glMultMatrixd( transform.constData() ); // XXX This will NOT work on ARM!

/* PIXEL */
/*
	// - Render from the VBO -
	// Bind the VBO
	lineVertexBuffer.bind();


	// Prepare client for rendering from the server (Graphics card)
	// Setup VBO access size for glDrawArrays
	shader->setAttributeArray(
		"inputVertex",
		GL_FLOAT,
		NULL,
		0,
		3 * sizeof( GLfloat ) );
	shader->enableAttributeArray("inputVertex");


	// Debug colour
	glColor3f(1.0f,1.0f,1.0f); // White 

	// Sanity read...this prevents spurious initial segfault TODO FIXME
	char tmp;
	if ( lineVertexBuffer.read( 0, &tmp, 1 ) )
	{
		// Draw MChar
		glDrawArrays( GL_QUADS, 0, 4 * 6 * totalPixels );
	}

	// - Cleanup -
	shader->disableAttributeArray("inputVertex");
	lineVertexBuffer.release();
*/	


/* TEXTURE */
	// - Render from the VBO -
	// Bind the VBO
	lineVertexBuffer.bind();


	// Prepare client for rendering from the server (Graphics card)
	// Setup VBO access size for glDrawArrays
	shader->setAttributeArray(
		"inputVertex",
		GL_FLOAT,
		NULL,
		3,
		8 * sizeof( GLfloat ) );
	shader->enableAttributeArray("inputVertex");


	// Setup VBO access of the TexCoords
	shader->setAttributeArray(
		"inputTexCoord",
		GL_FLOAT,
		((char*)NULL) + 3 * sizeof( GLfloat ), // Starts 3 elements after the start of the buffer
		2,
		8 * sizeof( GLfloat ) );
	shader->enableAttributeArray("inputTexCoord");


	// Setup VBO access of the animiation parameters (yes, we are ignoring secondary color)
	shader->setAttributeArray(
		"inputAnimationParams",
		GL_FLOAT,
		((char*)NULL) + 5 * sizeof( GLfloat ), // Starts 5 elements after the start of the buffer
		3,
		8 * sizeof( GLfloat ) );
	shader->enableAttributeArray("inputAnimationParams");


	// Debug colour
	glColor3f(1.0f,1.0f,1.0f); // White 

	// Sanity read...this prevents spurious initial segfault TODO FIXME
	char tmp;
	if ( lineVertexBuffer.read( 0, &tmp, 1 ) )
	{
		// Render a character at a time from the VBO
		for ( int c = 0; c < chars.size(); c++ )
		{
			// Bind texture
			chars[c]->bindTexture();

			// Draw MChar
			glDrawArrays( GL_QUADS, c * 4 * 6, 4 * 6 );
		}
	}

	// - Cleanup -
	shader->disableAttributeArray("inputVertex");
	shader->disableAttributeArray("inputTexCoord");
	shader->disableAttributeArray("inputAnimationParams");
	lineVertexBuffer.release();

	// Unapply transform
	glPopMatrix();
}


// Functions **************************************************************************************
void MLine::addChar( MChar *mchar, int pixelCount )
{
	// Given MChar and adds it to the end of the line
	// No checking is done for line size, this is assumed to be taken care of by the above layer
	chars.push_back( mchar );

	// Increase pixel count if given
	totalPixels += pixelCount;
}

void MLine::setChar( MChar *mchar, int index )
{
	chars[index] = mchar;
}

void MLine::dropEndChar( unsigned int number )
{
	// Don't attempt to drop too many
	unsigned int totalDrop = number > (unsigned int)chars.size() ? chars.size() : number;
	unsigned int start = chars.size() - totalDrop;

	// Cleanup object
	for ( int c = start; c < chars.size(); c++ )
		delete chars[c];

	// Remove items
	chars.remove( chars.size() - totalDrop, totalDrop );
}

