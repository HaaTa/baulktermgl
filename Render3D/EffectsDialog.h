// Jacob Alexander 2012

#ifndef __EFFECTSDIALOG_H
#define __EFFECTSDIALOG_H

// Qt Includes
#include <QDialog>

// Project Includes

namespace Ui {
	class Dialog;
}

class EffectsDialog : public QDialog
{
	Q_OBJECT

public:
	explicit EffectsDialog( QWidget *parent = 0 );
	~EffectsDialog();

	Ui::Dialog *ui;
private:
	
};


#endif

