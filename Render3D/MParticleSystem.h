// Jacob Alexander 2012

#ifndef __MPARTICLESYSTEM_H
#define __MPARTICLESYSTEM_H

// Qt Includes
#include <QColor>
#include <QVector>
#include <QVector3D>

// Project Includes


// Pre-declarations
class QGLWidget;


class MParticle
{
public:
	MParticle();
	MParticle( float life, float speed, QColor color, QVector3D gravity );
	~MParticle();

	// Variables

	// - Particle Attributes -
	bool active;
	float lifeRemaining;
	float totalLifetime;
	QColor color;
	QVector3D position;
	QVector3D direction;
	QVector3D gravity;
};


class MParticleSystem
{
public:
	MParticleSystem();
	MParticleSystem( float baseSpeed, float systemLifetime, int maxParticles, QVector3D position );
	~MParticleSystem();

	// Render Functions
	void updateSystem( int timeElapsed );
	void renderMParticleSystem( QGLWidget *renderer );

	// Modifiers
	void reset();

	// Accessors
	bool isFinished() const { return finished; }

private:
	// Functions
	void setup();

	// Variables
	QVector<MParticle> particles;

	QVector<GLfloat> particlePosition;
	QVector<GLfloat> particleColor;

	bool finished;
	float totalLifetime; // Particle System will be enabled this long
	float startSpeed;
	int totalParticles;
	int currentNumberOfParticles;

	QVector3D basePosition;

	QImage particleQImage;
	GLuint textures[1];

	QGLWidget *lastRenderer;
};



#endif

