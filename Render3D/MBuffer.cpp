// Jacob Alexander 2012

// Qt Includes
#include <QGLShaderProgram>

// OpenGL Includes
#include <GL/glu.h>

// Project Includes
#include <MBuffer.h>
#include <MSpline.h>


// Post/Pre ***************************************************************************************
MBuffer::MBuffer()
{
	// Create MSpline (B-Spline)
	mspline = new MSpline;

	// Setup initial zMax of the spline (i.e. how close to the camera the coords will get, or perhaps past)
	mspline->setZMax( 200 );

	// Spline Usage is initially disabled
	lineSplinePath = false;

	// No cursor events initially
	cursorChangeEvent = false;
	lastCursorPos = QPoint( 0,0 );
}

MBuffer::~MBuffer()
{
	// Cleanup MSpline
	delete mspline;
}


// Render Functions *******************************************************************************
void MBuffer::renderMBuffer( int start, int count, QGLWidget *renderer, QGLShaderProgram *shader )
{
	// Initially no animations scheduled
	particleAnimation = false;
	keyframeAnimation = false;

	// Make sure the end is <= to the number of lines
	int start_adjusted = start < 0 ? 0 : start;
	int end_adjusted = count + start_adjusted >= lines.size() ? lines.size() : count + start_adjusted;
	int lineWidthAdjustment = charHeight + lineSpacing;


	// Calculate spline for this render configuration
	// if ( usingSPLINETODO )
	{
		int totalLines = end_adjusted - start_adjusted;

		// Regenerates the spline if necessary
		mspline->setYMax( (totalLines + 1) * lineWidthAdjustment );
	}

	// Adjust for the current starting MLine
	QMatrix4x4 lineAdjustment;
	lineAdjustment.translate( 0, lineWidthAdjustment * -start_adjusted );
	lineAdjustment = transform * lineAdjustment;

	// Apply overall transform to the lines in the buffer
	glPushMatrix();
	glMultMatrixd( lineAdjustment.constData() ); // XXX This will NOT work on ARM!

	// Render each MLine within the bounds
	for ( int c = start_adjusted; c < end_adjusted; c++ )
	{
		// If characters need to be updated from the cache
		if ( lines[c].isDirty() )
		{
			lines[c].prepareMLine( renderer );
		}


		// Apply B-Spline Transformation to the current line
		QMatrix4x4 prevLineTransform;
		if ( lineSplinePath )
		{
			prevLineTransform = lines[c].getTransformation();

			// Set Z coord, given the current Y coord
			QMatrix4x4 newTransform = prevLineTransform;
			newTransform( 2,3 ) = mspline->zPosition( newTransform( 1,3 ) - (start_adjusted * lineWidthAdjustment) );

			lines[c].setTransformation( newTransform );
		}


		// Pass char width and height to shader
		shader->setUniformValue( "charWidth",  charWidth );
		shader->setUniformValue( "charHeight", charHeight );


		// Render Line to Screen
		lines[c].renderMLine( renderer, shader );


		// Unapply B-Spline Transformation
		if ( lineSplinePath ) lines[c].setTransformation( prevLineTransform );


		// Check the character to see if any animations need to be scheduled
		particleAnimation = particleAnimation || lines[c].doParticleAnimation();
		keyframeAnimation = keyframeAnimation || lines[c].doKeyframeAnimation();
	}

	// Unapply transform
	glPopMatrix();
}

void renderShadows( QGLWidget *renderer, QGLShaderProgram *shader /* lights */ )
{
	// The reason why volume shadows are not implemented.
	// Too much refactoring needed, and the effect won't look that impressive...
	

	// Re-process the vbo
	// Calculating the direction of the shadow using the light sources
	// Project the vector to infinity (or in this case to the skybox)
	// Draw front (end of the shadow) and the back (object that is intersecting the light source)
	// Fill the center using Quads

	// Do the above for front face culling, rear face culling, and then draw the shadow using the stencil buffer


	// To make more efficent, calculate the shadow intersections using a shader, saving to a texture map
	// The easiest way is to process each light separately

	// Once the texture of information is ready, create the volume shadows (this part would be prohibitively expensive with this program...)
}


// Functions **************************************************************************************
void MBuffer::addLine( MLine &line )
{
	// Add a line to the buffer
	lines.push_back( line );

	// Set the next line position
	lines.back().setTransformation( nextLineTransform );

	// Adjust the transform for the next added line
	nextLineTransform.translate( 0, charHeight + lineSpacing );
}

void MBuffer::popBackLine( int num )
{
	// Remove a number of lines from the back of the buffer
	for ( int c = 0; c < num; c++ )
		lines.pop_back();
}

