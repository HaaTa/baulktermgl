// Jacob Alexander 2012

// Qt Includes

// OpenGL Includes

// System Includes
#include <math.h>

// Project Includes
#include <MSpline.h>


// Post/Pre ***************************************************************************************
MSpline::MSpline()
{
	// Initialize Variables
	yMax = 100;
	zMax = 100;
	midpoint = 0.60;

	generate();
}

MSpline::~MSpline()
{
	clean();
}


// Functions **************************************************************************************
double MSpline::zPosition( double yPosition )
{
	// Select spline to use, based on position
	if ( yPosition < yMax * midpoint )
	{
		return gsl_spline_eval( spline1, yPosition, acc );
	}
	else
	{
		return gsl_spline_eval( spline2, yPosition, acc );
	}
}

void MSpline::setYMax( int yMax )
{
	// Regenerate if needed
	if ( yMax != this->yMax )
	{
		this->yMax = yMax;
		clean();
		generate();
	}
}

void MSpline::setZMax( int zMax )
{
	// Regenerate if needed
	if ( zMax != this->zMax )
	{
		this->zMax = zMax;
		clean();
		generate();
	}
}

void MSpline::generate()
{
	int N = 3;

	// Setup points on B-Spline
	double y1[3] = { 0.00, 0.25 * yMax, midpoint * yMax};
	double z1[3] = { 0.00, 0.20 * zMax, 0.15     * zMax};

	double y2[3] = {y1[2], 0.80 * yMax, 1.00 * yMax};
	double z2[3] = {z1[2], 0.40 * zMax, 0.00 * zMax};


	// Initialize spline from gsl using set points
	acc = gsl_interp_accel_alloc();
	const gsl_interp_type *t = gsl_interp_cspline_periodic; 
	spline1 = gsl_spline_alloc( t, N );
	spline2 = gsl_spline_alloc( t, N );
	gsl_spline_init( spline1, y1, z1, N );
	gsl_spline_init( spline2, y2, z2, N );
}

void MSpline::clean()
{
	gsl_spline_free( spline1 );
	gsl_spline_free( spline2 );
	gsl_interp_accel_free( acc );
}

