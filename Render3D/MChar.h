// Jacob Alexander 2012

#ifndef __MCHAR_H
#define __MCHAR_H

// Qt Includes
#include <QGLBuffer>
#include <QImage>
#include <QSize>
#include <QVector>

// OpenGL Includes
#include <GL/gl.h>

// Project Includes
#include <MPixel.h>
#include <Character.h>


// Pre-declarations
class QGLWidget;


class MChar
{
public:
	MChar();
	virtual ~MChar();

	// Copy Functions
	virtual MChar *copy( Character *characterStyled ) = 0;

	// Render Functions
	virtual void renderMChar( QGLWidget *renderer, QGLBuffer *bufferV, int offset, int index ) = 0;
	virtual void bindTexture() {}

	// Modifiers
	virtual void setColors( const Material &foreground, const Material &material ) = 0;

	void setTransformation( QMatrix4x4 &matrix ) {       transform = matrix; }
	void setCharacter( Character *character )    { this->character = character; }
	void setBold( bool useBold )                 {            bold = useBold; }
	void setUnderline( bool useUnderline )       {       underline = useUnderline; }

	virtual void setWidth( const size_t &width ) = 0;

	void setDirty()                                   { dirty = true; }
	void setCreationInstance( unsigned int instance ) { creationInstance = instance; }
	void setAnimationID( int animationID )            { this->animationID = animationID; }


	// Accessors
	Material getForeground() const { return foreground; }
	Material getBackground() const { return background; }
	bool     isBold()        const { return bold; }
	bool     isUnderlined()  const { return underline; }
	bool     isDirty()       const { return dirty; }

	QMatrix4x4 getTransformation() const { return transform; }

	QSize getSize() const { return size; }

	Character *getCharacterRepresentation() const { return character; }

	bool doParticleAnimation() const { return particleAnimation; }
	bool doKeyframeAnimation() const { return keyframeAnimation; }

protected:
	// Variables
	QSize size;
	Material foreground;
	Material background;

	QMatrix4x4 transform;

	Character *character;
	bool copiedCharacter;

	bool bold;
	bool underline;

	bool dirty;

	bool particleAnimation;
	bool keyframeAnimation;

	QGLWidget *lastRenderer;

	unsigned int creationInstance;

	int animationID;
};

class MCharPixel : public MChar
{
public:
	MCharPixel();
	virtual ~MCharPixel();

	// Copy Functions
	virtual MChar *copy( Character *characterStyled );

	// Render Functions
	virtual void renderMChar( QGLWidget *renderer, QGLBuffer *buffer, int offset, int index );

	// Modifiers
	virtual void setColors( const Material &foreground, const Material &material );

	void addPixel( const MPixel &pixel );

	virtual void setWidth( const size_t &width );

	// Accessors
	MPixel *getPixel( int index ) { return &pixels[index]; }

	int getPixelCount() const { return pixels.size(); }


private:
	// Variables
	QVector<MPixel> pixels;
};

class MCharTexture : public MChar
{
public:
	MCharTexture();
	virtual ~MCharTexture();

	// Copy Functions
	virtual MChar *copy( Character *characterStyled );

	// Render Functions
	virtual void renderMChar( QGLWidget *renderer, QGLBuffer *buffer, int offset, int index );
	virtual void bindTexture();

	// Modifiers
	virtual void setColors( const Material &foreground, const Material &material );

	void setTexture( QImage &image );

	virtual void setWidth ( const size_t &width );
	virtual void setHeight( const size_t &height );

private:
	// Variables
	QImage texture;

	GLuint textures[1];

	// Functions
	void determineVertices( GLfloat *vertices, int index );
};

#endif

