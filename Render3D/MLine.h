// Jacob Alexander 2012

#ifndef __MLINE_H
#define __MLINE_H

// Qt Includes
#include <QGLBuffer>
#include <QMatrix4x4>
#include <QVector>

// Project Includes
#include <MChar.h>


// Pre-declarations
class QGLShaderProgram;
class QGLWidget;


class MLine
{
public:
	MLine();
	~MLine();

	// Render Functions
	void prepareMLine( QGLWidget *renderer );
	void  renderMLine( QGLWidget *renderer, QGLShaderProgram *shader );

	// Modifiers
	void setTransformation( QMatrix4x4 &matrix ) { transform = matrix; }

	void addChar( MChar *mchar, int pixelCount = 0 );
	void setChar( MChar *mchar, int index );

	void dropEndChar( unsigned int number );

	void setDirty() { dirty = true; }


	// Accessors
	QMatrix4x4 getTransformation() const { return transform; }

	MChar *getChar( int index ) { return chars[index]; }

	int getCharCount() const { return chars.size(); }

	bool doParticleAnimation() const { return particleAnimation; }
	bool doKeyframeAnimation() const { return keyframeAnimation; }

	bool isDirty() const { return dirty; }

private:
	// Variables
	QVector<MChar*> chars;

	QMatrix4x4 transform;

	bool particleAnimation;
	bool keyframeAnimation;

	int prevLineRenderLength;

	QGLBuffer lineVertexBuffer;

	bool dirty;

	int textureUnits;

	int totalPixels;
};


#endif

