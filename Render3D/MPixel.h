// Jacob Alexander 2012

#ifndef __MPIXEL_H
#define __MPIXEL_H

// Qt Includes
#include <QGLBuffer>
#include <QMatrix4x4>

// Project Includes
#include <Material.h>


// Pre-declarations
class QGLWidget;


class MPixel
{
public:
	enum Type {
		FOREGROUND,
		BACKGROUND,
	};

	MPixel() {}
	MPixel( const QMatrix4x4 &matrix, Type type ) : transform( matrix ), type( type ) {}
	MPixel( const Material &material, const QMatrix4x4 &matrix, Type type )
		: material( material ), transform( matrix ), type( type ) {}
	~MPixel() {}

	// Render Functions
	void renderMPixel( QGLWidget *renderer, QGLBuffer *buffer, int offset, int x, int y );

	// Modifiers
	void setMaterial      ( Material    material ) { this->material = material; }
	void setTransformation( QMatrix4x4 &matrix   ) { transform      = matrix;   }
	void setType          ( Type        type     ) { this->type     = type;     }

	// Accessors
	Material   getMaterial()       const { return material;  }
	QMatrix4x4 getTransformation() const { return transform; }
	Type       getType()           const { return type;      }

private:
	// Functions
	void drawUnitCube();
	void drawUnitCube( GLfloat *vertices, int x, int y );

	// Variables
	Material   material;
	QMatrix4x4 transform;
	Type       type;
};


#endif

