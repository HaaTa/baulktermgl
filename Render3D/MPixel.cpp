// Jacob Alexander 2012

// Qt Includes

// OpenGL Includes
#include <GL/glu.h>

// Project Includes
#include <MPixel.h>

// Render Functions *******************************************************************************
void MPixel::renderMPixel( QGLWidget *renderer, QGLBuffer *buffer, int offset, int x, int y )
{
	// Calculate the vertices (including line offset, which is added in x)
	//GLfloat pixelVertices[totalVertices * 3];
	//drawUnitCube( &pixelVertices[0], x, y );



	GLfloat pixelVertices[] = {
	// Front face
	1.0f + x, 1.0f + y, 1.0f,
	0.0f + x, 1.0f + y, 1.0f,
	0.0f + x, 0.0f + y, 1.0f,
	1.0f + x, 0.0f + y, 1.0f,
	// Top face
	1.0f + x, 1.0f + y, 0.0f,
	0.0f + x, 1.0f + y, 0.0f,
	0.0f + x, 1.0f + y, 1.0f,
	1.0f + x, 1.0f + y, 1.0f,

	// Ri1.0f face
	1.0f + x, 1.0f + y, 0.0f,
	1.0f + x, 1.0f + y, 1.0f,
	1.0f + x, 0.0f + y, 1.0f,
	1.0f + x, 0.0f + y, 0.0f,

	// Left face
	0.0f + x, 1.0f + y, 1.0f,
	0.0f + x, 1.0f + y, 0.0f,
	0.0f + x, 0.0f + y, 0.0f,
	0.0f + x, 0.0f + y, 1.0f,

	// Bottom face
	0.0f + x, 0.0f + y, 0.0f,
	1.0f + x, 0.0f + y, 0.0f,
	1.0f + x, 0.0f + y, 1.0f,
	0.0f + x, 0.0f + y, 1.0f,

	// Back face
	0.0f + x, 1.0f + y, 0.0f,
	1.0f + x, 1.0f + y, 0.0f,
	1.0f + x, 0.0f + y, 0.0f,
	0.0f + x, 0.0f + y, 0.0f
};



	// XXX Must write in large enough chunks...or things refuse to copy...
	// Add each of the vertices to the VBO
	const int fields = 3 * sizeof( GLfloat );
	const int totalVertices = 25;
	buffer->write( offset, &pixelVertices[0], totalVertices * fields );

				//GLfloat tmp[2*totalVertices*3];
				//buffer->read( offset - totalVertices * fields, &tmp[0], 2*totalVertices*fields );
				//for ( int c = 0; c <= 2*totalVertices*3; c+= 6 )
//qDebug()<<pixelVertices[c]<<pixelVertices[c+1]<<pixelVertices[c+2]<<pixelVertices[c+3]<<pixelVertices[c+4]<<pixelVertices[c+5]
    //qDebug()    << " ---- " <<tmp[c]<<tmp[c+1]<<tmp[c+2]<<tmp[c+3]<<tmp[c+4]<<tmp[c+5];
//	qDebug() << "DONE";
	

	/*
	// Apply current transformation to the model state before render pixel cube
	glPushMatrix();
	glMultMatrixd( transform.constData() ); // XXX This will NOT work on ARM!

	// Apply pixel material
	// TODO <- Use proper material
	switch ( type )
	{
	case FOREGROUND:
		glColor3f(0.0f,1.0f,0.0f); // Green
		break;
	case BACKGROUND:
		glColor3f(1.0f,0.5f,0.0f); // Orange
		break;
	}

	// Render pixel 1x1x1 cube
	drawUnitCube();

	// Unapply pixel transformation
	glPopMatrix();
	*/
}


void MPixel::drawUnitCube()
{
	// Unit cube
	glBegin( GL_QUADS );

	// Front face
	glVertex3d( 1.0,  1.0,  1.0 );
	glVertex3d( 0.0,  1.0,  1.0 );
	glVertex3d( 0.0,  0.0,  1.0 );
	glVertex3d( 1.0,  0.0,  1.0 );
	// Top face
	glVertex3d( 1.0,  1.0,  0.0 );
	glVertex3d( 0.0,  1.0,  0.0 );
	glVertex3d( 0.0,  1.0,  1.0 );
	glVertex3d( 1.0,  1.0,  1.0 );

	// Right face
	glVertex3d( 1.0,  1.0,  0.0 );
	glVertex3d( 1.0,  1.0,  1.0 );
	glVertex3d( 1.0,  0.0,  1.0 );
	glVertex3d( 1.0,  0.0,  0.0 );

	// Left face
	glVertex3d( 0.0,  1.0,  1.0 );
	glVertex3d( 0.0,  1.0,  0.0 );
	glVertex3d( 0.0,  0.0,  0.0 );
	glVertex3d( 0.0,  0.0,  1.0 );

	// Bottom face
	glVertex3d( 0.0,  0.0,  0.0 );
	glVertex3d( 1.0,  0.0,  0.0 );
	glVertex3d( 1.0,  0.0,  1.0 );
	glVertex3d( 0.0,  0.0,  1.0 );

	// Back face
	glVertex3d( 0.0,  1.0,  0.0 );
	glVertex3d( 1.0,  1.0,  0.0 );
	glVertex3d( 1.0,  0.0,  0.0 );
	glVertex3d( 0.0,  0.0,  0.0 );

	glEnd();
}

void MPixel::drawUnitCube( GLfloat *vertices, int x, int y )
{
	// (x;y;z;) vertices
	// Front face
	*vertices++ = 1.0 + x; *vertices++ = 1.0 + y; *vertices++ = 1.0;
	*vertices++ = 0.0 + x; *vertices++ = 1.0 + y; *vertices++ = 1.0;
	*vertices++ = 0.0 + x; *vertices++ = 0.0 + y; *vertices++ = 1.0;
	*vertices++ = 1.0 + x; *vertices++ = 0.0 + y; *vertices++ = 1.0;
	// Top face
	*vertices++ = 1.0 + x; *vertices++ = 1.0 + y; *vertices++ = 0.0;
	*vertices++ = 0.0 + x; *vertices++ = 1.0 + y; *vertices++ = 0.0;
	*vertices++ = 0.0 + x; *vertices++ = 1.0 + y; *vertices++ = 1.0;
	*vertices++ = 1.0 + x; *vertices++ = 1.0 + y; *vertices++ = 1.0;

	// Ri1.0 face
	*vertices++ = 1.0 + x; *vertices++ = 1.0 + y; *vertices++ = 0.0;
	*vertices++ = 1.0 + x; *vertices++ = 1.0 + y; *vertices++ = 1.0;
	*vertices++ = 1.0 + x; *vertices++ = 0.0 + y; *vertices++ = 1.0;
	*vertices++ = 1.0 + x; *vertices++ = 0.0 + y; *vertices++ = 0.0;

	// Left face
	*vertices++ = 0.0 + x; *vertices++ = 1.0 + y; *vertices++ = 1.0;
	*vertices++ = 0.0 + x; *vertices++ = 1.0 + y; *vertices++ = 0.0;
	*vertices++ = 0.0 + x; *vertices++ = 0.0 + y; *vertices++ = 0.0;
	*vertices++ = 0.0 + x; *vertices++ = 0.0 + y; *vertices++ = 1.0;

	// Bottom face
	*vertices++ = 0.0 + x; *vertices++ = 0.0 + y; *vertices++ = 0.0;
	*vertices++ = 1.0 + x; *vertices++ = 0.0 + y; *vertices++ = 0.0;
	*vertices++ = 1.0 + x; *vertices++ = 0.0 + y; *vertices++ = 1.0;
	*vertices++ = 0.0 + x; *vertices++ = 0.0 + y; *vertices++ = 1.0;

	// Back face
	*vertices++ = 0.0 + x; *vertices++ = 1.0 + y; *vertices++ = 0.0;
	*vertices++ = 1.0 + x; *vertices++ = 1.0 + y; *vertices++ = 0.0;
	*vertices++ = 1.0 + x; *vertices++ = 0.0 + y; *vertices++ = 0.0;
	*vertices++ = 0.0 + x; *vertices++ = 0.0 + y; *vertices++ = 0.0;
}

