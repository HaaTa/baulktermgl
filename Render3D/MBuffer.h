// Jacob Alexander 2012

#ifndef __MBUFFER_H
#define __MBUFFER_H

// Qt Includes
#include <QPoint>
#include <QVector>

// Project Includes
#include <MLine.h>
#include <MCharCache.h>


// Pre-declarations
class QGLShaderProgram;
class QGLWidget;
class MSpline;

class MBuffer
{
public:
	MBuffer();
	~MBuffer();

	// Render Functions
	void renderMBuffer( int start, int count, QGLWidget *renderer = 0, QGLShaderProgram *shader = 0 );
	void renderMBufferPixel( int start, int count, QGLWidget *renderer = 0, QGLShaderProgram *shader = 0 );
	void renderShadows( QGLWidget *renderer = 0, QGLShaderProgram *shader = 0 );


	// Modifiers
	void setTransformation( QMatrix4x4 &matrix )    { transform = matrix; }
	void setCharHeight( int charHeight )            { this->charHeight = charHeight; }
	void setCharWidth( int charWidth )              { this->charWidth  = charWidth; }
	void setLineSpacing( int lineSpacing )          { this->lineSpacing = lineSpacing; }

	void addLine( MLine &line );
	void popBackLine( int num = 1 );

	void setLinePathSpline( bool enable )           { lineSplinePath = enable; }

	void updateCursorPos( const QPoint &cursorPos ) { lastCursorPos = cursorPos; cursorChangeEvent = true; }


	// Accessors
	QMatrix4x4 getTransformation() const { return transform; }

	MLine *getLine( int index )          { return &lines[index]; }

	int getLineCount() const             { return lines.size(); }

	QPoint getLastCursorPos() const      { return lastCursorPos; }

	bool doParticleAnimation() const     { return particleAnimation; }
	bool doKeyframeAnimation() const     { return keyframeAnimation; }

private:
	// Variables
	QVector<MLine> lines;

	QMatrix4x4 transform;
	QMatrix4x4 nextLineTransform;

	int charHeight;
	int charWidth;
	int lineSpacing;

	bool particleAnimation;
	bool keyframeAnimation;

	MSpline *mspline;
	bool lineSplinePath;

	QPoint lastCursorPos; // Col,Row representation
	bool cursorChangeEvent;

	

	// MPixel VBO Control Variables
};


#endif

