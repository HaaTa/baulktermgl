// Jacob Alexander 2012

// Qt Includes
#include <QChar>
#include <QFontMetrics>
#include <QImage>
#include <QPainter>

// OpenGL Includes

// Project Includes
#include "LineFont.h"
#include <Material.h>
#include <MCharCache.h>


// Defines
#define REPCHAR   "ABCDEFGHIJKLMNOPQRSTUVWXYZ" \
                  "abcdefgjijklmnopqrstuvwxyz" \
                  "0123456789./+@"


// Graphics Renderering Functions *****************************************************************

/**
 A table for emulating the simple (single width) unicode drawing chars.
 It represents the 250x - 257x glyphs. If it's zero, we can't use it.
 if it's not, it's encoded as follows: imagine a 5x5 grid where the points are numbered
 0 to 24 left to top, top to bottom. Each point is represented by the corresponding bit.

 Then, the pixels basically have the following interpretation:
 _|||_
 -...-
 -...-
 -...-
 _|||_

where _ = none
      | = vertical line.
      - = horizontal line.
 */


enum LineEncode
{
    TopL  = (1<<1),
    TopC  = (1<<2),
    TopR  = (1<<3),

    LeftT = (1<<5),
    Int11 = (1<<6),
    Int12 = (1<<7),
    Int13 = (1<<8),
    RightT = (1<<9),

    LeftC = (1<<10),
    Int21 = (1<<11),
    Int22 = (1<<12),
    Int23 = (1<<13),
    RightC = (1<<14),

    LeftB = (1<<15),
    Int31 = (1<<16),
    Int32 = (1<<17),
    Int33 = (1<<18),
    RightB = (1<<19),

    BotL  = (1<<21),
    BotC  = (1<<22),
    BotR  = (1<<23)
};

static void drawLineChar(QPainter& paint, int x, int y, int w, int h, uchar code)
{
    //Calculate cell midpoints, end points.
    int cx = x + w/2;
    int cy = y + h/2;
    int ex = x + w - 1;
    int ey = y + h - 1;

    quint32 toDraw = LineChars[code];

    //Top _lines:
    if (toDraw & TopL)
        paint.drawLine(cx-1, y, cx-1, cy-2);
    if (toDraw & TopC)
        paint.drawLine(cx, y, cx, cy-2);
    if (toDraw & TopR)
        paint.drawLine(cx+1, y, cx+1, cy-2);

    //Bot _lines:
    if (toDraw & BotL)
        paint.drawLine(cx-1, cy+2, cx-1, ey);
    if (toDraw & BotC)
        paint.drawLine(cx, cy+2, cx, ey);
    if (toDraw & BotR)
        paint.drawLine(cx+1, cy+2, cx+1, ey);

    //Left _lines:
    if (toDraw & LeftT)
        paint.drawLine(x, cy-1, cx-2, cy-1);
    if (toDraw & LeftC)
        paint.drawLine(x, cy, cx-2, cy);
    if (toDraw & LeftB)
        paint.drawLine(x, cy+1, cx-2, cy+1);

    //Right _lines:
    if (toDraw & RightT)
        paint.drawLine(cx+2, cy-1, ex, cy-1);
    if (toDraw & RightC)
        paint.drawLine(cx+2, cy, ex, cy);
    if (toDraw & RightB)
        paint.drawLine(cx+2, cy+1, ex, cy+1);

    //Intersection points.
    if (toDraw & Int11)
        paint.drawPoint(cx-1, cy-1);
    if (toDraw & Int12)
        paint.drawPoint(cx, cy-1);
    if (toDraw & Int13)
        paint.drawPoint(cx+1, cy-1);

    if (toDraw & Int21)
        paint.drawPoint(cx-1, cy);
    if (toDraw & Int22)
        paint.drawPoint(cx, cy);
    if (toDraw & Int23)
        paint.drawPoint(cx+1, cy);

    if (toDraw & Int31)
        paint.drawPoint(cx-1, cy+1);
    if (toDraw & Int32)
        paint.drawPoint(cx, cy+1);
    if (toDraw & Int33)
        paint.drawPoint(cx+1, cy+1);

}

// TODO integrate into both MCharTexture and MCharPixel rendering
void MCharCache::drawLineCharString( QPainter& painter, int x, int y, const QString& str, const Character* attributes )
{
	const QPen& currentPen = painter.pen();

	if ( attributes->rendition & RE_BOLD )
	{
		QPen boldPen(currentPen);
		boldPen.setWidth(3);
		painter.setPen( boldPen );
	}

	for (int i=0 ; i < str.length(); i++)
	{
		uchar code = str[i].cell();
		if (LineChars[code])
		drawLineChar(painter, x + (fontWidth*i), y, fontWidth, fontHeight, code);
	}

	painter.setPen( currentPen );
}


// Post/Pre ***************************************************************************************
MCharCache::MCharCache()
{
}

MCharCache::~MCharCache()
{
}


// Character Build / Retrieval Functions **********************************************************
MChar *MCharCache::requestChar( Character *characterStyled, RenderType renderType )
{
	// Determine unicode character
	quint16 character = characterStyled->character;

	// Setup bold and underline
	bool useBold      = characterStyled->rendition & RE_BOLD      || font.bold() || characterStyled->isBold( colorTable );
	bool useUnderline = characterStyled->rendition & RE_UNDERLINE || font.underline();

	// Depending on the character render type, attempt to find the cached render of the character
	switch ( renderType )
	{
	case RENDER_Pixel: {
		QHash<quint16, MCharPixel> *cache;

		// Choose cache
		// Depending on whether underlined, bold, a combination or none, select respective cache
		     if ( useBold && useUnderline ) cache = &cachePixelUnderline;
		else if ( useBold )                 cache = &cachePixelBold;
		else if ( useUnderline )            cache = &cachePixelUnderline;
		else                                cache = &cachePixel;

		// Search hash table to see if cache has already been made of the character
		if ( !cache->contains( character ) )
		{
			// No cache available of the character, make one
			(*cache)[character] = buildPixelChar( characterStyled, useBold, useUnderline );
		}

		// Colour Character in the cache before sending
		colourAdjust( characterStyled, &((*cache)[character]) );

		return &((*cache)[character]); }

	case RENDER_Texture: {
		QHash<quint16, MCharTexture> *cache;

		// Choose cache
		// Depending on whether underlined, bold, a combination or none, select respective cache
		     if ( useBold && useUnderline ) cache = &cacheTextureUnderline;
		else if ( useBold )                 cache = &cacheTextureBold;
		else if ( useUnderline )            cache = &cacheTextureUnderline;
		else                                cache = &cacheTexture;

		// Search hash table to see if cache has already been made of the character
		if ( !cache->contains( character ) )
		{
			// No cache available of the character, make one
			(*cache)[character] = buildTextureChar( characterStyled, useBold, useUnderline );
		}

		// Colour Character in the cache before sending
		colourAdjust( characterStyled, &((*cache)[character]) );

		return &((*cache)[character]); }

	default:
		qCritical() << "Invalid Render Type for MChar";
		
		return &cacheTexture[character];
	}
}

MCharTexture MCharCache::buildTextureChar( Character *characterStyled, bool useBold, bool useUnderline )
{
	// Setup the "modified" font with bold and underline flags
	QFont curFont = font;
	curFont.setBold( useBold );
	curFont.setUnderline( useUnderline );

	// First generate the image to hold the texture
	QImage image( fontWidth, fontHeight, QImage::Format_ARGB32_Premultiplied );
	image.fill( QColor( 255,255,255,255 ) );

	// Character Line Strings
	// TODO - Makes NCurses look fancy

	// Next use QPainter to draw the character onto the image
	QPainter painter;
	painter.begin( &image );
	painter.setPen( QColor( 0,0,0,255 ) ); // Paint text as black
	painter.setFont( curFont );
	painter.drawText( 0, fontOverline, QString( QChar( characterStyled->character ) ) );
	painter.end();

	// Create the MChar
	MCharTexture mchar;
	mchar.setWidth ( fontWidth );
	mchar.setHeight( fontHeight );
	mchar.setBold( useBold );
	mchar.setUnderline( useUnderline );

	// Copy the given character (needed for the dirty mask)
	Character *copyChar = new Character;
	*copyChar = *characterStyled;
	mchar.setCharacter( copyChar );

	// Set Texture
	mchar.setTexture( image );

	return mchar;
}

MCharPixel MCharCache::buildPixelChar( Character *characterStyled, bool useBold, bool useUnderline )
{
	// First generate an image for analyzing the character
	QImage image( fontWidth, fontHeight, QImage::Format_Mono );
	image.fill( 1 ); // Fill base image as white (background detection)

	// Next use QPainter to draw the character onto the image
	QPainter painter;
	painter.begin( &image );
	painter.setPen( QColor( 0,0,0 ) ); // Paint text as black (foreground detection)
	painter.setFont( font );
	painter.drawText( 0, fontOverline, QString( QChar( characterStyled->character ) ) );
	painter.end();

	// Create the MChar
	MCharPixel mchar;
	mchar.setCharacter( characterStyled );

	// Initial Identity matrix
	QMatrix4x4 pixelMatrix;

#define PIXEL_BACK  0xFFFFFFFF
#define PIXEL_FRONT 0xFF000000
	// Iterate over the pixels to determine which are the foreground and background pixels
	for ( int y = 0; y < fontHeight; y++ )
	{
		for ( int x = 0; x < fontWidth; x++ )
		{
			// White pixel (background)
			if ( image.pixel( x, y ) == PIXEL_BACK )
			{
				mchar.addPixel( MPixel( pixelMatrix, MPixel::BACKGROUND ) );
			}
			// Black pixel (foreground)
			else
			{
				mchar.addPixel( MPixel( pixelMatrix, MPixel::FOREGROUND ) );
			}

			// Adjust matrix (translate next pixel to the right 1 - x direction)
			pixelMatrix.translate( 1, 0 );
		}

		// Adjust matrix (reset horizontal translation, translate down 1 - y direction)
		pixelMatrix.translate( -fontWidth, 1 );
	}

	mchar.setWidth( fontWidth );

	return mchar;
}

void MCharCache::colourAdjust( Character *characterStyled, MChar *curChar )
{
	const CharacterColor& textColor = characterStyled->foregroundColor;
	const CharacterColor& backColor = characterStyled->backgroundColor;
	QColor foreground = textColor.color( colorTable );
	QColor background = backColor.color( colorTable );

	// If this is a cursor character, use cursor colours
	if ( characterStyled->rendition & RE_CURSOR )
	{
		// TODO Support other cursors than BlockCursor

		// Check if there is a cursor colour
		if ( cursorColor.isValid() )
		{
			foreground = background;
			background = cursorColor;
		}
		// TODO Better fallback scheme than reverse?
		else
		{
			foreground = background;
			background = textColor.color( colorTable );
		}
	}

	curChar->setColors( Material( foreground ), Material( background ) );
}


// Cache Maintenance Functions ********************************************************************
void MCharCache::clearCache()
{
	// Clear out each of the caches
	cachePixel.clear();
	cachePixelBold.clear();
	cachePixelUnderline.clear();
	cachePixelBoldUnderline.clear();

	cacheTexture.clear();
	cacheTextureBold.clear();
	cacheTextureUnderline.clear();
	cacheTextureBoldUnderline.clear();
}


// Font Functions *********************************************************************************
void MCharCache::setFont( const QFont &font )
{
	// Set font
	this->font = font;

	// Determined the used font height
	QFontMetrics fm( font );
	fontHeight = fm.height();

	// waba TerminalDisplay 1.123:
	// "Base character width on widest ASCII character. This prevents too wide
	//  characters in the presence of double wide (e.g. Japanese) characters."
	// Get the width from representative normal width characters
	fontWidth = qRound( (double)fm.width(REPCHAR) / (double)strlen(REPCHAR) );

	// Make sure the font width is at least 1, but warn, as this is incorrect for monospaced fonts
	if ( fontWidth < 1 )
	{
		fontWidth = 1;
		qWarning() << "Font width of less than 1 detected.";
	}

	// Make sure font is NOT anti-aliased (XXX for now set to Bitmap fonts only)
	this->font.setStyleStrategy( QFont::PreferBitmap );

	// Make sure kerning is DISABLED
	this->font.setKerning( false );

	// Set font overline
	fontOverline = fm.overlinePos();

	// TODO FIXME - Why?
	fontHeight -= 1;
	fontOverline -= 1;
}

