// Jacob Alexander 2012

// Qt Includes
#include <QGLWidget>

// OpenGL Includes
#include <GL/glu.h>

// System Includes
#include <stdlib.h>
#include <time.h>

// Project Includes
#include <MParticleSystem.h>



/**************************************************************************************************
 *
 *                                    MParticleSystem
 *
 **************************************************************************************************/


// Post/Pre ***************************************************************************************
MParticleSystem::MParticleSystem( float baseSpeed, float systemLifetime, int maxParticles, QVector3D position )
{
	// Initialize Variables
	totalLifetime  = systemLifetime;
	startSpeed     = baseSpeed;
	totalParticles = maxParticles;
	basePosition   = position;

	setup();
}

MParticleSystem::MParticleSystem()
{
	// Initialize Variables
	totalLifetime  =  5.0f;
	startSpeed     =  2.0f;
	totalParticles = 30;
	basePosition   = QVector3D( 0,0,0 );

	setup();
}

MParticleSystem::~MParticleSystem()
{
	// Cleanup texture
	if ( lastRenderer != 0 && textures[0] != 0 )
	{
		lastRenderer->deleteTexture( textures[0] );
		textures[0] = 0;
	}
}


// Render Functions *******************************************************************************
void MParticleSystem::updateSystem( int timeElapsed )
{
	// Convert timeElapsed into seconds from (milliseconds)
	float agedTime = timeElapsed / 1000.0f;

	// Assume no particles to render unless they are found
	currentNumberOfParticles = 0;

	// Iterate over each of the particles
	for ( int c = 0; c < particles.size(); c++ )
	{
		// Only continue to process particle stats if it is active
		if ( !particles[c].active )
			continue;


		// Age particle
		particles[c].lifeRemaining -= agedTime;


		// Check if particle has died
		if ( particles[c].lifeRemaining <= 0.0f )
		{
			// Disable, so checking is faster
			particles[c].active = false;

			// TODO reset particle, so it can be displayed again
		}
		// Update particle parameters, and prepare to render particle
		else
		{
			// Calculate need speed/direction, using gravity vector
			particles[c].direction += particles[c].gravity;

			// Calculate distance traveled from current position over elapsed time
			particles[c].position += particles[c].direction * agedTime;

			// Prepare to render
			currentNumberOfParticles++;
			particlePosition[currentNumberOfParticles * 3]     = particles[c].position.x() + basePosition.x();
			particlePosition[currentNumberOfParticles * 3 + 1] = particles[c].position.y() + basePosition.y();
			particlePosition[currentNumberOfParticles * 3 + 2] = particles[c].position.z() + basePosition.z();

			particleColor[currentNumberOfParticles * 4]     = particles[c].color.red()   / 255.0f;
			particleColor[currentNumberOfParticles * 4 + 1] = particles[c].color.green() / 255.0f;
			particleColor[currentNumberOfParticles * 4 + 2] = particles[c].color.blue()  / 255.0f;
			particleColor[currentNumberOfParticles * 4 + 3] = particles[c].color.alpha() / 255.0f;
		}
	}

	// If no particles were rendered, this system has died...erm is done
	if ( currentNumberOfParticles <= 0 )
		finished = true;
}

void MParticleSystem::renderMParticleSystem( QGLWidget *renderer )
{
	// Setup
	glEnable( GL_BLEND );
	glDisable( GL_DEPTH_TEST );
	glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
	glEnable( GL_TEXTURE_2D );


	// Point Sprite Setup
	glEnable( GL_POINT_SPRITE );
	glTexEnvi( GL_POINT_SPRITE, GL_COORD_REPLACE, GL_TRUE );
	glPointSize( 2 );


	// Load the texture if it hasn't been already
	if ( textures[0] == 0 )
	{
		glGenTextures( 1, &textures[0] );
		textures[0] = renderer->bindTexture( particleQImage );

		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
	}

	// Bind the texture
	glBindTexture( GL_TEXTURE_2D, textures[0] );


	// Setup for rendering from arrays
	glEnableClientState( GL_VERTEX_ARRAY );
	glVertexPointer( 3, GL_FLOAT, 0, particlePosition.data() );
	glEnableClientState( GL_COLOR_ARRAY );
	glColorPointer( 4, GL_FLOAT, 0, particleColor.data() );


	// Render particles
	glDrawArrays( GL_POINTS, 0, currentNumberOfParticles );


	// Cleanup
	glDisableClientState( GL_VERTEX_ARRAY );
	glDisableClientState( GL_COLOR_ARRAY );
	glEnable( GL_DEPTH_TEST );
	glDisable( GL_BLEND );
	glTexEnvi( GL_POINT_SPRITE, GL_COORD_REPLACE, GL_FALSE );
	glDisable( GL_TEXTURE_2D );
	glDisable( GL_POINT_SPRITE );
}


// Functions **************************************************************************************
void MParticleSystem::reset()
{
	finished = false;

	// Make sure enough particles are available
	particles.       resize( totalParticles );
	particlePosition.resize( totalParticles * 3 );
	particleColor.   resize( totalParticles * 4 );
	currentNumberOfParticles = 0;

	// Initialize all of the particles
	for ( int c = 0; c < particles.size(); c++ )
	{
		particles[c].active        = true;
		particles[c].lifeRemaining = 2 + rand() % 2; // 2 to 4 seconds
		particles[c].totalLifetime = particles[c].lifeRemaining;
		particles[c].position      = QVector3D( 0,0,0 );
		particles[c].direction     = QVector3D( rand() % 128 - 64, rand() % 128 - 64, rand() % 128 - 64 ); // Random direction (Magnitude specifies speed)
		particles[c].gravity       = QVector3D( 0, 9.81, 1.2 ); // Fall down and away
		particles[c].color         = QColor( rand() % 255, rand() % 255, rand() % 255, rand() % 255 ); // Random color + random alpha
	}
}

void MParticleSystem::setup()
{
	// Seed random numbers
	srand( time(NULL) );

	reset();

	// Load in Texture
	// TODO Make selectable
	QString particleImage = "particle.bmp";
	QString path = "../Particle/";

	// TODO Make sure image exists
	QImage particleQImage = QImage( path + particleImage );
	textures[0] = 0;

	lastRenderer = 0;
}





/**************************************************************************************************
 *
 *                                          MParticle
 *
 **************************************************************************************************/


// Post/Pre ***************************************************************************************
MParticle::MParticle()
{
	// Initialize Variables
	active        = true;
	position      = QVector3D( 0,0,0 );
	direction     = QVector3D( 0,0,0 );
	gravity       = QVector3D( 0,0,0 );
	lifeRemaining = 0;
	color         = QColor( 0,0,0 );
}

MParticle::MParticle( float life, float speed, QColor color, QVector3D gravity )
{
	// Initialize Variables
	active    = true;
	position  = QVector3D( 0,0,0 );
	direction = QVector3D( 0,0,0 );

	// Set variables
	lifeRemaining = life;
	this->color   = color;
	this->gravity = gravity;
}

MParticle::~MParticle()
{
}


// Functions **************************************************************************************


