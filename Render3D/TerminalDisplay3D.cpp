// Jacob Alexander

// Debug
#include <iostream>
#include "Screen.h"

// Qt
#include <QtGui/QApplication>
#include <QtGui/QBoxLayout>
#include <QtGui/QKeyEvent>
#include <QtCore/QEvent>
#include <QtCore/QTime>
#include <QtCore/QFile>
#include <QtGui/QGridLayout>
#include <QtGui/QLabel>
#include <QtGui/QLayout>
#include <QtGui/QPainter>
#include <QtGui/QPixmap>
#include <QtGui/QStyle>
#include <QtCore>
#include <QtGui>

// Project Includes
#include "TerminalDisplay3D.h"
#include "ScreenWindow.h"
#include "TerminalCharacterDecoder.h"
#include "konsole_wcwidth.h"
#include "EffectsDialog.h"
#include "ui_EffectsDialog.h"


// Defines
#define __SFILE__ \
(strrchr(__FILE__,'/') \
? strrchr(__FILE__,'/')+1 \
: __FILE__ \
)



/* ------------------------------------------------------------------------- */
/*                                                                           */
/*                                   Font                                    */
/*                                                                           */
/* ------------------------------------------------------------------------- */

/*
   The VT100 has 32 special graphical characters. The usual vt100 extended
   xterm fonts have these at 0x00..0x1f.

   QT's iso mapping leaves 0x00..0x7f without any changes. But the graphicals
   come in here as proper unicode characters.

   We treat non-iso10646 fonts as VT100 extended and do the requiered mapping
   from unicode to 0x00..0x1f. The remaining translation is then left to the
   QCodec.
*/

void TerminalDisplay3D::setVTFont(const QFont& f)
{
}

void TerminalDisplay3D::setFont(const QFont& f)
{
}



/* ------------------------------------------------------------------------- */
/*                                                                           */
/*                         Constructor / Destructor                          */
/*                                                                           */
/* ------------------------------------------------------------------------- */
TerminalDisplay3D::TerminalDisplay3D( QWidget *parent ) : GLWidget( parent )
{
	// terminal applications are not designed with Right-To-Left in mind,
	// so the layout is forced to Left-To-Right
	setLayoutDirection( Qt::LeftToRight );

	basicSetup( this ); // <-- TerminalDisplay.h/cpp

	// Pass the color tables to the font renderer
	mcharcache.setColorTable( _colorTable );
	mcharcache.setCursorColor( _cursorColor ); // Warning, this option only sets at startup... TODO Fix this

	// Setup render line variables of glWidget
	renderLines = 0;
	renderLineCurrent = 0;

	setMouseTracking(true);

	setFocusPolicy( Qt::WheelFocus );

	// Create effects dialog window
	effectsDialog = new EffectsDialog();
	Ui::Dialog *dialog = effectsDialog->ui;

	// Connect dialog box signals
	connect( dialog->TerminalRadio,    SIGNAL( clicked() ), this, SLOT( setTerminalMouseMode() ) );
	connect( dialog->TranslationRadio, SIGNAL( clicked() ), this, SLOT( setTranslationMouseMode() ) );
	connect( dialog->RotateRadio,      SIGNAL( clicked() ), this, SLOT( setRotationMouseMode() ) );

	connect( dialog->OrthographicRadio, SIGNAL( clicked() ), this, SLOT( setOrthographicProjectionMode() ) );
	connect( dialog->PerspectiveRadio,  SIGNAL( clicked() ), this, SLOT( setPerspectiveProjectionMode() ) );

	connect( dialog->Particle,     SIGNAL( clicked() ), this, SLOT( setParticleAnimation() ) );
	connect( dialog->KeyframeAnim, SIGNAL( clicked() ), this, SLOT( setKeyframeAnimation() ) );
	connect( dialog->SplineAnim,   SIGNAL( clicked() ), this, SLOT( setTextPathAnimation() ) );

	connect( dialog->Skybox,       SIGNAL( clicked() ), this, SLOT( setSkybox() ) );
	connect( dialog->SelectSkybox, SIGNAL( clicked() ), this, SLOT( setSkyboxImage() ) );
	connect( dialog->Reflection,   SIGNAL( clicked() ), this, SLOT( setReflection() ) );

	connect( dialog->Glow,       SIGNAL( clicked() ), this, SLOT( setGlow() ) );
	connect( dialog->MotionBlur, SIGNAL( clicked() ), this, SLOT( setMotionBlur() ) );

	connect( dialog->VolumeShadows, SIGNAL( clicked() ), this, SLOT( setVolumeShadows() ) );

	connect( dialog->TextureFontRadio, SIGNAL( clicked() ), this, SLOT( setTextureFont() ) );
	connect( dialog->PixelFontRadio,   SIGNAL( clicked() ), this, SLOT( setPixelFont() ) );

	// enable input method support
	setAttribute( Qt::WA_InputMethodEnabled, true );
}


TerminalDisplay3D::~TerminalDisplay3D()
{
	basicCleanup(); // <-- TerminalDisplay.h/cpp

	// Cleanup dialog box
	delete effectsDialog;
}



/* ------------------------------------------------------------------------- */
/*                                                                           */
/*                             Display Operations                            */
/*                                                                           */
/* ------------------------------------------------------------------------- */

void TerminalDisplay3D::setOpacity(qreal opacity)
{
    QColor color(_blendColor);
    color.setAlphaF(opacity);

    // enable automatic background filling to prevent the display
    // flickering if there is no transparency
    if ( color.alpha() == 255 ) 
    {
        //setAutoFillBackground(true);
    }
    else
    {
        //setAutoFillBackground(false);
    }

    _blendColor = color.rgba();
}

// Main update function
// This is where all of the character events get propagated to the screen
// e.g. the character positions passed onto the OpenGL character rendering datastructure
//      afterwards, a redraw event is issued to display the new characters
void TerminalDisplay3D::updateImage() 
{
	// Check to make sure there actually is a screen window to retrieve the character data from
	if ( !_screenWindow )
		return;

	_screenWindow->resetScrollCount(); // Reset scroll amount (is this necessary? TODO

	Character* newcharbuf = _screenWindow->getImage();
	int lines = _screenWindow->windowLines();
	int columns = _screenWindow->windowColumns();
	int currentLine = _screenWindow->currentLine();


	// Konsole-Like "Image" Refresh
	// Based off the code in TerminalDisplay2D.cpp
	// "Scrolls" gl buffer accordingly, scans the new gl buffer window for "dirty" characters
	// NOTE, dirty mask has been integrated into the main scan so the dirty flag serves a different purpose now
	// Modifies the "dirty" characters, then re-renders

	// NOTE:
	// - Potential Enhancements -
	// * Integrate more closely with ScreenWindow and Screen such that they direcly modify the gl buffer

	// Scroll gl buffer
	renderLineCurrent = currentLine;

	for ( int y = 0; y < lines; y++ )
	{
		// Determine gl/mbuffer line
		int mbufferY = y + currentLine;

		// Make sure the line exists
		if ( mbufferY >= mbuffer.getLineCount() )
		{
			MLine tempLine;
			mbuffer.addLine( tempLine );
		}


		// Retrieve the line
		MLine *curLine = mbuffer.getLine( mbufferY );
		const Character* const newLine = &newcharbuf[ y * columns ];


		// Iterate over the characters in the line
		for ( int x = 0; x < columns; x++ )
		{
			// Get character from newcharbuf
			Character *newChar = (Character*)&newLine[ x ];

			// Skip if there is no data at that point
			if ( !newChar->character )
			{
				continue;
			}

			// Check to see if character is already there, if so don't update the buffer
			if ( x < qMin( columns, curLine->getCharCount() )
			  && newLine[ x ] == *curLine->getChar( x )->getCharacterRepresentation() )
			{
				continue;
			}

			// Get character
			MChar *curChar = mcharcache.requestChar( newChar, fontRenderType );


			// Determine if the cursor position has changed
			bool cursorChange = false;
			if ( QPoint( x,y ) != mbuffer.getLastCursorPos() && newChar->rendition & RE_CURSOR )
			{
				mbuffer.updateCursorPos( QPoint( x,y ) );
				cursorChange = true;

				// Add particle effect at this position if enabled
				if ( particleAnimationEnabled )
				{
					int fontHeight = mcharcache.getFontHeight();
					int fontWidth  = mcharcache.getFontWidth();

					particleSystems.append( MParticleSystem( 2.0f, 30.0f, 30,
						QVector3D( x * fontWidth, y * fontHeight, 0 ) ) );
					// TODO cleanup old particle systems (probably not here)
					// TODO enable re-render
				}
			}


			// Add character to line if no character exists at that point
			if ( x >= curLine->getCharCount() )
			{
				// Copy the given character to the end of the line
				curLine->addChar( curChar->copy( newChar ) );
			}
			// Update the character
			else
			{
				// Clean the given character
				delete curLine->getChar( x );

				// Update character position
				curLine->setChar( curChar->copy( newChar ), x );
			}

			curLine->setDirty(); // Indicate to render, that there is a dirty MChar in the MLine

			// Set creation index, this is used for per character animations
			// It is pushed all the way to the VBO
			// Then the shader will use this value to appropriately draw the animation
			curLine->getChar( x )->setCreationInstance( (unsigned int) currentRenderInstance );

			// Enable basic rotations
			if ( keyframeAnimationEnabled && cursorChange )
			{
				curLine->getChar( x )->setAnimationID( 1 );
			}
		}

		// Drop any extra columns
		curLine->dropEndChar( curLine->getCharCount() - columns );
	}


	// Re-render
	updateGL();
}

void TerminalDisplay3D::blinkCursorEvent()
{
}

/* ------------------------------------------------------------------------- */
/*                                                                           */
/*                                  Resizing                                 */
/*                                                                           */
/* ------------------------------------------------------------------------- */

void TerminalDisplay3D::resizeTerminalScreen( int width, int height )
{
	// Convert pixels (assume mapping is the same as screenWindow) to characters
	_lines   = qMax( 1, height / mcharcache.getFontHeight() );
	_columns = qMax( 1, width  / mcharcache.getFontWidth()  );

	// Propagate the lines to the OpenGL renderer
	renderLines = _lines;

	// Propagate size change to terminal control
	eventObj()->changedContentSizeSignalEvent( width, height );

	// Inform the screenWindow about the line change
	if ( _screenWindow )
		_screenWindow->setWindowLines( _lines );
}


/* ------------------------------------------------------------------------- */
/*                                                                           */
/*                                Scrollbar                                  */
/*                                                                           */
/* ------------------------------------------------------------------------- */


void TerminalDisplay3D::updateLineProperties()
{
	if ( !_screenWindow ) 
		return;

	_lineProperties = _screenWindow->getLineProperties();    
}

void TerminalDisplay3D::setScroll(int cursor, int slines)
{
}

void TerminalDisplay3D::setScrollBarPosition(ScrollBarPosition position)
{
}

/* ------------------------------------------------------------------------- */
/*                                                                           */
/*                                 Mouse                                     */
/*                                                                           */
/* ------------------------------------------------------------------------- */
void TerminalDisplay3D::setUsesMouse(bool on)
{
	_mouseMarks = on;
	displayWidget->setCursor( _mouseMarks ? Qt::IBeamCursor : Qt::ArrowCursor );
}

void TerminalDisplay3D::wheelEvent( QWheelEvent *event )
{
	if (event->orientation() != Qt::Vertical)
		return;

	// TODO Add scroll bar

	// TODO Add terminal mouse wheel input (e.g. vim scroll)
	// Scroll view window
	if ( _mouseMarks )
	{
		int numDegrees = event->delta() / 8;
		int numSteps = numDegrees / 15;
		//_screenWindow->scrollBy( ScreenWindow::ScrollLines, numSteps );
		renderLineCurrent += numSteps;

		// Velocity Counter
		wheelVelocityCounter += numSteps;

		// Make sure not trying to render past the bounds
		/*
		if ( renderLineCurrent > renderLines )
			renderLineCurrent = renderLines;

		if ( renderLineCurrent < 0 )
			renderLineCurrent = 0;
		*/

		updateGL();
		event->accept();
	}
	// Terminal Application scroll (TODO Unfinished)
	else
	{
		int charLine = 1;
		int charColumn = 1;

		// TODO selection...this is very hard...
		//getCharacterPosition( event->pos() , charLine , charColumn );

		eventObject->mouseSignalEvent( event->delta() > 0 ? 4 : 5,
		                               charColumn + 1,
		                               charLine + 1,
		                               //charLine + 1 + _scrollBar->value() - _scrollBar->maximum(),
		                               0 );

		qWarning() << "Application scroll, not fully implemented...";
	}
}

void TerminalDisplay3D::getCharacterPosition(const QPoint& widgetPoint,int& line,int& column) const
{
	/* TODO This needs to be implemented/ported for mouse control
	column = (widgetPoint.x() + _fontWidth / 2 - contentsRect().left() - _leftMargin) / _fontWidth;
	line   = (widgetPoint.y()                  - contentsRect().top()  -  _topMargin) / _fontHeight;

	// Make sure positions are positive
	if ( line   < 0 ) line   = 0;
	if ( column < 0 ) column = 0;

	if ( line >= _usedLines )
		line = _usedLines - 1;

	// the column value returned can be equal to _usedColumns, which
	// is the position just after the last character displayed in a line.
	//
	// this is required so that the user can select characters in the right-most
	// column (or left-most for right-to-left input)
	if ( column > _usedColumns )
		column = _usedColumns;
	*/
}


/* ------------------------------------------------------------------------- */
/*                                                                           */
/*                               Clipboard                                   */
/*                                                                           */
/* ------------------------------------------------------------------------- */

void TerminalDisplay3D::copyClipboard()
{
}

void TerminalDisplay3D::pasteClipboard()
{
  emitSelection(false,false);
}

void TerminalDisplay3D::pasteSelection()
{
  emitSelection(true,false);
}

#undef KeyPress

void TerminalDisplay3D::emitSelection( bool useXselection, bool appendReturn ) {
}

/* ------------------------------------------------------------------------- */
/*                                                                           */
/*                                Keyboard                                   */
/*                                                                           */
/* ------------------------------------------------------------------------- */

void TerminalDisplay3D::setFlowControlWarningEnabled( bool enable )
{
	_flowControlWarningEnabled = enable;
	
	// if the dialog is currently visible and the flow control warning has 
	// been disabled then hide the dialog
	if (!enable)
		outputSuspended(false);
}

void TerminalDisplay3D::bell(const QString&)
{
}

void TerminalDisplay3D::setFixedSize(int cols, int lins)
{
}

void TerminalDisplay3D::outputSuspended(bool suspended)
{
}

void TerminalDisplay3D::keyPressEvent( QKeyEvent* event )
{
	qDebug("%s %d keyPressEvent and key is %d", __SFILE__, __LINE__, event->key());

	bool emitKeyPressSignal = true;

	// XonXoff flow control
	if (event->modifiers() & Qt::ControlModifier && _flowControlWarningEnabled)
	{
		if ( event->key() == Qt::Key_S ) {
			qDebug("%s %d keyPressEvent, output suspended", __SFILE__, __LINE__);
			eventObject->flowControlKeyPressedEvent(true /*output suspended*/);
		}
		else if ( event->key() == Qt::Key_Q ) {
			qDebug("%s %d keyPressEvent, output enabled", __SFILE__, __LINE__);
			eventObject->flowControlKeyPressedEvent(false /*output enabled*/);
		}
	}

	// Hotkeys
	switch ( event->key() )
	{
	case Qt::Key_F1:
		effectsDialog->setVisible( !effectsDialog->isVisible() );
		break;
	case Qt::Key_F2: // Terminal Mode
		effectsDialog->ui->TerminalRadio->click();
		break;
	case Qt::Key_F3: // Translation Mode
		effectsDialog->ui->TranslationRadio->click();
		break;
	case Qt::Key_F4: // Rotation Mode
		effectsDialog->ui->RotateRadio->click();
		break;
	case Qt::Key_F5: // Orthographic Projection Mode
		effectsDialog->ui->OrthographicRadio->click();
		break;
	case Qt::Key_F6: // Perspective Projection Mode
		effectsDialog->ui->PerspectiveRadio->click();
		break;
	}

	// Keyboard-based navigation
	if ( event->modifiers() == Qt::ShiftModifier )
	{
		bool update = true;

		if ( event->key() == Qt::Key_PageUp )
		{
			qDebug("%s %d pageup", __SFILE__, __LINE__);
			_screenWindow->scrollBy( ScreenWindow::ScrollPages , -1 );
		}
		else if ( event->key() == Qt::Key_PageDown )
		{
			qDebug("%s %d pagedown", __SFILE__, __LINE__);
			_screenWindow->scrollBy( ScreenWindow::ScrollPages , 1 );
		}
		else if ( event->key() == Qt::Key_Up )
		{
			qDebug("%s %d keyup", __SFILE__, __LINE__);	
			_screenWindow->scrollBy( ScreenWindow::ScrollLines , -1 );
		}
		else if ( event->key() == Qt::Key_Down )
		{
			qDebug("%s %d keydown", __SFILE__, __LINE__);	
			_screenWindow->scrollBy( ScreenWindow::ScrollLines , 1 );
		}
		else
		{
			update = false;
		}

		if ( update )
		{
			qDebug("%s %d updating", __SFILE__, __LINE__);	
			_screenWindow->setTrackOutput( _screenWindow->atEndOfOutput() );

			updateLineProperties();
			updateImage();

			// do not send key press to terminal
			emitKeyPressSignal = false;
		}
	}

	_screenWindow->setTrackOutput( true );

	_actSel = 0; // Key stroke implies a screen update, so TerminalDisplay won't
	// know where the current selection is.

	if (_hasBlinkingCursor) 
	{
		_blinkCursorTimer->start(BLINK_DELAY);
		if (_cursorBlinking) blinkCursorEvent(); else _cursorBlinking = false;
	}

	if ( emitKeyPressSignal )
	{
		eventObject->keyPressedSignalEvent(event);
	}

	event->accept();
}


/* ------------------------------------------------------------------------- */
/*                                                                           */
/*                                 Skybox                                    */
/*                                                                           */
/* ------------------------------------------------------------------------- */

void TerminalDisplay3D::setSkyboxImage()
{
	// Open a file dialog and set the string path
	skyboxImagePath = QFileDialog::getOpenFileName(
		0,
		tr("Select Skybox Image"),
		QDir::homePath(),
		tr("Image Files (*.png *.jpg *.bmp)") );

	// Update the UI with the filename
	// TODO enable/disable
	effectsDialog->ui->SkyboxSelected->setText( QFileInfo( skyboxImagePath ).fileName() );
}


