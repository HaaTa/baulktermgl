// Jacob Alexander 2012

#ifndef TERMINALDISPLAY3D_H
#define TERMINALDISPLAY3D_H

// Qt
#include <QtGui/QWidget>

// Project Includes
#include "TerminalDisplay.h"
#include <glwidget.h>


// Pre-Declarations
class QDrag;
class QDragEnterEvent;
class QDropEvent;
class QTimer;
class QEvent;
class QFrame;
class QGridLayout;
class QKeyEvent;
class QShowEvent;
class QHideEvent;
class QWidget;

class EffectsDialog;


class TerminalDisplay3D : public GLWidget, public TerminalDisplay
{
	Q_OBJECT

public:
	/** Constructs a new terminal display widget with the specified parent. */
	TerminalDisplay3D( QWidget *parent = 0 );
	~TerminalDisplay3D();

	QFont getVTFont() { return font(); }

	/** 
	 * Reimplemented.  Has no effect.  Use setVTFont() to change the font
	 * used to draw characters in the display.
	 */
	virtual void setFont(const QFont &);

	/** 
	 * Sets the font used to draw the display.  Has no effect if @p font
	 * is larger than the size of the display itself.    
	 */
	virtual void setVTFont(const QFont& font);

	/** Sets the opacity of the terminal display. */
	void setOpacity(qreal opacity);

	virtual void setFixedSize(int cols, int lins);
	virtual void emitSelection(bool useXselection,bool appendReturn);
	virtual void setScroll(int cursor, int lines);
	virtual void setScrollBarPosition(ScrollBarPosition position);

public slots:
	/** 
	 * Causes the terminal display to fetch the latest character image from the associated
	 * terminal screen ( see setScreenWindow() ) and redraw the display.
	 */
	virtual void updateImage(); 
	virtual void bell(const QString& message);
	virtual void outputSuspended(bool suspended);
	virtual void updateLineProperties();
	virtual void setUsesMouse(bool usesMouse);
	virtual void copyClipboard();
	virtual void pasteClipboard();
	virtual void pasteSelection();
	virtual void setFlowControlWarningEnabled(bool enabled);
	virtual bool usesMouse() const { return _mouseMarks; }

protected slots:
	void blinkCursorEvent();

protected:
	// Qt Re-Implemented Functions
	virtual void keyPressEvent( QKeyEvent *event );
	virtual void wheelEvent( QWheelEvent *event );

	// Internal Re-Implemented Functions
	virtual void resizeTerminalScreen( int width, int height );

	// Functions
	void getCharacterPosition( const QPoint &widgetPoint, int &line, int &column ) const;

	// Variables
	EffectsDialog *effectsDialog;

private slots:
	void setSkyboxImage();

};

#endif

