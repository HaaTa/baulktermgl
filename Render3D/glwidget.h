// Jacob Alexander 2012

#ifndef __GLWIDGET_H
#define __GLWIDGET_H

// Qt Includes
#include <QGLFramebufferObject>
#include <QGLWidget>
#include <QList>

// Project Includes
#include <MBuffer.h>
#include <MCharCache.h>
#include <MParticleSystem.h>


// Pre-declarations
class QGLShader;
class QGLShaderProgram;


class GLWidget : public QGLWidget
{
	Q_OBJECT

public:
	enum MouseMode {
		MOUSE_Terminal,
		MOUSE_Translation,
		MOUSE_Rotation,
	};

	enum ProjectionMode {
		PROJ_Perspective,
		PROJ_Orthographic,
	};

	GLWidget( QWidget *parent = 0 );
	~GLWidget();

	// Accessors
	QSize minimumSizeHint() const;
	QSize sizeHint() const;

public slots:
	void setTerminalMouseMode();
	void setTranslationMouseMode();
	void setRotationMouseMode();

	void setOrthographicProjectionMode();
	void setPerspectiveProjectionMode();

	void setParticleAnimation();
	void setKeyframeAnimation();
	void setTextPathAnimation();

	void setMotionBlur();
	void setVolumeShadows();
	void setReflection();
	void setSkybox();
	void setGlow();
	//void setGlowAmount();

	void setTextureFont();
	void setPixelFont();

signals:

protected:
	// Qt Re-implemented functions
	void initializeGL();
	void paintGL();
	void resizeGL( int width, int height );
	void mousePressEvent( QMouseEvent *event );
	void mouseReleaseEvent( QMouseEvent *event );
	void mouseMoveEvent( QMouseEvent *event );

	// Internal use functions
	virtual void resizeTerminalScreen( int width, int height ) = 0;

	// Variables

	QString skyboxImagePath;

	int wheelVelocityCounter;

	// - Character/Render Datastructure Variables -
	MBuffer mbuffer;
	MCharCache mcharcache;
	int renderLines; // Sets the maximum number of lines rendered to the screen
	int renderLineCurrent; // Starting line position for the render

	// - Render Scheduler Variables -
	unsigned long long currentRenderInstance;
	unsigned long long    prevRenderInstance;

	bool reRenderOnUpdate;

	bool particleAnimationEnabled;
	bool keyframeAnimationEnabled;

	// - Particle System Variables -
	QList<MParticleSystem> particleSystems;

	// - Font Render Variables -
	MCharCache::RenderType fontRenderType;

private:
	// Functions
	void mouseCameraTranslationControl( QMouseEvent *event );
	void mouseCameraRotationControl( QMouseEvent *event );

	void adjustProjection( int width, int height );
	void adjustCameraTransformation();

	void renderSkybox( bool blackBox = false );
	void loadSkyboxTexture();

	void loadShaders();
	void loadShader( QGLShaderProgram **program, QGLShader **vertex, QGLShader **fragment, QString vertexName, QString fragmentName );
	void cleanupShaders();

	void updateFramebufferObjects( int width, int height );
	void cleanupFramebufferObjects();
	void renderFramebuffer( QGLFramebufferObject *fbo, QGLFramebufferObject *fbo2 = 0, QGLShaderProgram *shader = 0 );

	// Variables
	MouseMode mouseMode;
	ProjectionMode projMode;

	bool skyboxEnabled;
	bool skyboxLoaded;
	GLuint skyboxTextures[6];
	bool reflectionEnabled;

	bool volumeShadowsEnabled;

	bool glowEnabled;
	bool motionBlurEnabled;


	// - Shader Program Variables -
	QGLShaderProgram *currentShader;

	QGLShaderProgram *basicShader;
	QGLShader        *basicVertexShader1, *basicVertexShader2, *basicVertexShader3, *basicFragmentShader;
	QGLShader        *keyframeVertexShader;
	QGLShaderProgram *horizGlowShader;
	QGLShader        *horizGlowFragmentShader;
	QGLShaderProgram *vertGlowShader;
	QGLShader        *vertGlowFragmentShader;
	QGLShaderProgram *vertMotionShader;
	QGLShader        *vertMotionFragmentShader;
	QGLShaderProgram *reflectionShader;
	QGLShader        *reflectionVertexShader;
	QGLShader        *reflectionFragmentShader;


	// - Framebuffer Object Variables -
	QGLFramebufferObject *blurFramebufferHoriz;
	QGLFramebufferObject *blurFramebufferVert;
	QGLFramebufferObject *blurFramebufferMotion;
	QGLFramebufferObject *reflectionFramebuffer;


	// - Render Scheduler Variables -
	QTimer *renderInstanceCounter;

	bool textPathAnimationEnabled;


	// - Mouse Control Variables -
	bool mouseLBut_enabled;
	bool mouseMBut_enabled;
	bool mouseRBut_enabled;

	double mouseXCurrent;
	double mouseYCurrent;
	double mouseZCurrent;

	double mouseZoomCurrent;

	double cameraZoom;

	double cameraXMovement;
	double cameraYMovement;
	double cameraZMovement;

	double cameraXRotation;
	double cameraYRotation;
	double cameraZRotation;

private slots:
	// Render Event Scheduler Functions
	void updateTimeInstance();
};


#endif

