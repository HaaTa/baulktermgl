// Jacob Alexander 2012

#ifndef __MSPLINE_H
#define __MSPLINE_H

// Qt Includes

// GNU Scientific Includes
#include <gsl/gsl_errno.h>
#include <gsl/gsl_spline.h>

// Project Includes



class MSpline
{
public:
	MSpline();
	~MSpline();

	// Modifiers
	// TODO Modifier for control points, and midpoint
	void setYMax( int yMax );
	void setZMax( int zMin );

	// Accessors
	double zPosition( double yPosition );

private:
	// Functions
	void generate();
	void clean();

	// Variables
	gsl_spline *spline1;
	gsl_spline *spline2;

	gsl_interp_accel *acc;

	int yMax;
	int zMax;
	double midpoint;
};





#endif

