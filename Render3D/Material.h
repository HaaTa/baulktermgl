// Jacob Alexander 2012

#ifndef __MATERIAL_H
#define __MATERIAL_H

// Qt Includes
#include <QColor>

// Project Includes


class Material
{
public:
	Material();
	Material( const QColor &color ) : color( color ) {}
	~Material();

	// Modifiers
	void setColor( const QColor &color ) { this->color = color; }

	// Accessors
	QColor getColor() const { return color; }

private:
	// Variables
	QColor color;
};





#endif

