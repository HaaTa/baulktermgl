// Jacob Alexander 2012

#ifndef __MCHARCACHE_H
#define __MCHARCACHE_H

// Qt Includes
#include <QFont>
#include <QHash>

// Project Includes
#include <MChar.h>
#include "ColorTables.h"


class MCharCache
{
public:
	enum RenderType {
		RENDER_Pixel,
		RENDER_Texture,
	};

	MCharCache();
	~MCharCache();

	// Modifiers
	void setFont( const QFont &font );

	void setColorTable( ColorEntry *colorTable ) { this->colorTable  = colorTable; }
	void setCursorColor( QColor cursorColor )    { this->cursorColor = cursorColor; }

	void clearCache();


	// Accessors
	QFont getFont() const { return font; }

	int getFontHeight() { return fontHeight; }
	int getFontWidth () { return fontWidth;  }


	// Functions
	MChar *requestChar( Character *characterStyled, RenderType renderType );

private:
	// Functions
	void colourAdjust( Character *characterStyled, MChar *curChar );
	MCharPixel   buildPixelChar  ( Character *characterStyled, bool useBold, bool useUnderline );
	MCharTexture buildTextureChar( Character *characterStyled, bool useBold, bool useUnderline );

	void drawLineCharString( QPainter &painter, int x, int y, const QString &str, const Character *characterStyled );

	// Variables

	// - Caches -
	QHash<quint16, MCharPixel>   cachePixel;
	QHash<quint16, MCharPixel>   cachePixelBold;
	QHash<quint16, MCharPixel>   cachePixelBoldUnderline;
	QHash<quint16, MCharPixel>   cachePixelUnderline;

	QHash<quint16, MCharTexture> cacheTexture;
	QHash<quint16, MCharTexture> cacheTextureBold;
	QHash<quint16, MCharTexture> cacheTextureBoldUnderline;
	QHash<quint16, MCharTexture> cacheTextureUnderline;

	// - Font Properties -
	ColorEntry *colorTable;
	QFont font;
	QColor cursorColor;

	int fontHeight;
	int fontWidth;
	int fontOverline;
};


#endif

